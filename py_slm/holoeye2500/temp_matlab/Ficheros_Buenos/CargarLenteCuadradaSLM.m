%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FUNCI�N PARA CARGAR EL MAPA DE FASE DE JAVIER DE LA LENTE DE FRESNEL EXTENDI�NDOLO A
% TRANSMITANCIA 1 FUERA DE LA LENTE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [matriz_mapa_fase,inicio_x,fin_x,inicio_y,fin_y]=CargarLenteCuadradaSLM(mapafase)

% 22/4/08: Funciona
% 19/5/08: cambio sobre c�mo rellenar la matriz de ceros (LM)

% 20/5/08: Modificaci�n para quitar la barra de men�, etc y que ocupe toda
% la pantalla

% 3/09/08: Modificaci�n del archivo para que, a partir de la matriz generada con el programa de Javier 
% se genera la matriz de uint8 RGB de 24 bits necesaria para que funcione el programa de Java que
% carga la matriz en la pantalla virtual





% size_SLM_x es el tama�o del modulador en x: 1024
% size_SLM_x es el tama�o del modulador en y: 768
% coordenadas_pant_virtual: [-1 -1025 1024 768]
% coordenadas_pant_normal: [1 1 1024 768]


size_SLM_x=1024;
size_SLM_y=768;
[size_lente_x,size_lente_y]=size(mapafase);



% CREO LA MATRIZ EXTENDIDA

%load 'mapafase.mat'; % Fase de 240x240

centro_x_ventana=(size_SLM_x/2)-1; % Centros en x,y.
centro_y_ventana=(size_SLM_y/2)-1;

inicio_x=centro_x_ventana-(size_lente_x/2); % Defino los tama�os de la ventana y supongo que la lente tiene un n�mero de p�xeles par
fin_x=centro_x_ventana+(size_lente_x/2)-1; % Hay que incluir al cero de la ventana
inicio_y=centro_y_ventana-(size_lente_y/2);
fin_y=centro_y_ventana+(size_lente_y/2)-1;

matriz_mapa_fase=zeros(size_SLM_x,size_SLM_y);                  % La matriz inicial es todo ceros


matriz_mapa_fase(inicio_x:fin_x,inicio_y:fin_y)=mapafase;        % Relleno la matriz en el centro con la de Javier.

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%






% size_lente_x 240
% size_lente_y 240

%Vector_x_Red_Blaze=[1:1:size_SLM_x];
%Vector_y_Red_Blaze=[1:1:size_SLM_y];

%[matriz_mapa_fase,Theta,X_Modulador,Y_Modulador]=Red_Blaze(p,Vector_x_Red_Blaze,Vector_y_Red_Blaze); % La matriz inicial es una red Blaze de periodo p
%disp(Theta); % Saca en pantalla el valor del �ngulo deflectado en grados.

% CARGO LA MATRIZ EN LA PANTALLA VIRTUAL

%if not(isempty(coordenadas_pant_virtual))
%    if isempty(ID)
%        ID=figure;
%    else
%        ID=figure(ID);
%    end

    
    
    
  % Modificaci�n para meter una red Blaz� y quitar la barra de men� etc 
    
    % Figura
    
    
%    image(matriz_mapa_fase);colormap gray;
    
    %set(ID,'Menubar','none');       
    %set(ID,'Toolbar','none');    % Quita el men�
    %axis off                     % Quita los ejes
    %set(ID,'Color',[1,1,1])      % Pone el background en blanco
    %axes('position',[0 0 1 1]);  % Pone todo la figura en negro excepto la lente de Fresnel 
    %set(gcf,'Position',[1025 1 1024 768]);   % [left bottom width height]



