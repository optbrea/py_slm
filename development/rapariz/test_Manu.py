# !/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import time

import cv2
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
import scipy.misc
from scipy import ndimage

import lab_slm
from Ejercicios_TFG import Ejercicios
from diffractio import degrees, mm, um
from diffractio.scalar_masks_XY import Scalar_mask_XY
from diffractio.scalar_sources_XY import Scalar_source_XY

s = 1.

# Se puede cambiar el parametro de inicio para que haga la focalización inicial
SLM = lab_slm.lab_SLM(inicio=False, veloc=20 * mm / s)
# t = Scalar_mask_XY(SLM.x0, SLM.y0, SLM.wavelength)
w = Scalar_source_XY(SLM.x0 / 10, SLM.y0 / 10, SLM.wavelength)
w.plane_wave(A=1.)
# t.lens(r0=(0 * um, 0 * um), radius=(10 * mm, 10 * mm), focal=(300 * mm, 300 * mm))
q = Ejercicios(SLM.x0, SLM.y0, SLM.wavelength)
q.hiper_ellipse(r0=(0 * um, 0 * um), radius=(1 * mm, 1 * mm),
                angle=0 * degrees, n=0.5)
Q = q * w
Q1 = Q.RS(z=10 * mm, new_field=True)
Q1.draw_profile()

del(SLM)
