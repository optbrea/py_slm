% programa para generar el mapa de fases de una fzp cuadrada trabajando en
% reflexion
function mapafase=generarZonasLente(nzonasmaximo,nombreArchivo)
%save parametrosfzpcuadrada xm fases f lambdad nivelfase errornivelfase

if isempty(nombreArchivo)
    nombreArchivo='lentecuadrada.mat';
end
load(nombreArchivo);

nniveles=256; % numero de niveles de gris

if isempty(nzonasmaximo)
    nzonasmaximo=101;
end

periodo=19e-6; % 19 micras de periodo (es el periodo de la malla)
nhor=1024; % numero de puntos en la direccion horizontal
nver=768; % numero de puntos en la direccion vertical


% ajuste de las fases a una variacion lineal (esto es solo posible cuando
% tenemos un perfil cuadrado)

faseslineal=unwrap(fases);
indicezona=[2:1:length(fases)]';
p=polyfit(indicezona,faseslineal(2:length(fases)),1);
pendiente=p(1);
fasesajustadas=faseslineal;
fasesajustadas(2:nzonasmaximo)=[2:nzonasmaximo]*p(1)+p(2);
nivelesfaseajustados=fasesajustadas*nniveles./(2.*pi);
% valores normalizados a los niveles de digitalizacion


% offset de los niveles de fase y discretizacion de los mismos a un numero
% de niveles dado

%nivelesfase=nivelfase-min(nivelfase);
%nivelesfase=round(nivelesfase.*nniveles/(2*pi));
%nivelesfaserepetidos=nivelesfase(2:6);

% asignacion de las fases en ciclos a las zonas consecutivas
%
%repeticiones=ceil(nzonasmaximo/5);
%for jj=1:repeticiones-1
%    nivelesfase=[nivelesfase nivelesfaserepetidos];
%end


indices=zeros(nver/2,1);
indicepixellimitezona=zeros(nzonasmaximo,1);
indicepixellimitezona(1)=1;
for jj=2:nzonasmaximo
    indicepixellimitezona(jj)=floor(xm(jj)./periodo);
    % proporciona el indice del pixel en el que esta el limite de la zona jj-1
    fraccionzonaanterior(jj)=xm(jj)./periodo-indicepixellimitezona(jj);
    % proporciona la fraccion de pixel que esta en la zona anterior jj-1
    indices(indicepixellimitezona(jj-1):indicepixellimitezona(jj))=jj-1;
    lineafase(indicepixellimitezona(jj-1):indicepixellimitezona(jj))=nivelesfaseajustados(jj-1);
    % con esta asignacion estamos colocando valores de fase a todos los
    % pixeles, habra que corregir aquellos pixeles en los que estamos
    % compartiendo zonas.
end
diagonal=lineafase;

%%
jj=2;
while jj<=nzonasmaximo
    indicepixellimites=find(indicepixellimitezona(jj:nzonasmaximo)==indicepixellimitezona(jj));
    indicepixellimites=indicepixellimites+indicepixellimitezona(jj)-1;
    nlimitesenpixel=length(indicepixellimites);
    if nlimitesenpixel==1
        lineafase(indicepixellimites(1))=nivelesfaseajustados(jj-1).*fraccionzonaanterior(jj)+...
            nivelesfaseajustados(jj).*(1-fraccionzonaanterior(jj));
        diagonal(indicepixellimites(1))=nivelesfaseajustados(jj-1).*fraccionzonaanterior(jj).^2+...
            nivelesfaseajustados(jj).*(1-fraccionzonaanterior(jj).^2);

        jj=jj+1;

    end
    if nlimitesenpixel==2
        lineafase(indicepixellimites(1))=...
            nivelesfaseajustados(jj-1).*fraccionzonaanterior(jj)+...
            nivelesfaseajustados(jj).*(fraccionzonaanterior(jj+1)-fraccionzonaanterior(jj))+...
            nivelesfaseajustados(jj+1).*(1-fraccionzonaanterior(jj+1));
        diagonal(indicepixellimites(1))=...
            nivelesfaseajustados(jj-1).*fraccionzonaanterior(jj).^2+...
            nivelesfaseajustados(jj).*(fraccionzonaanterior(jj+1).^2-fraccionzonaanterior(jj).^2)+...
            nivelesfaseajustados(jj+1).*(1-fraccionzonaanterior(jj+1).^2);
        jj=jj+2;
    end
    %pause
end

%%
lineafase=round(mod(lineafase,256));
diagonal=round(mod(diagonal,256));
mapafase1c=zeros(length(lineafase),length(lineafase));

for ii=1:length(lineafase)
    for jj=1:ii
        mapafase1c(ii,jj)=lineafase(ii);
        mapafase1c(jj,ii)=lineafase(ii);
    end
    mapafase1c(ii,ii)=diagonal(ii);
end

%%
mapafase=zeros(2*length(lineafase),2*length(lineafase));
mapafase(1:length(lineafase),1:length(lineafase))=rot90(mapafase1c,2);
mapafase(1:length(lineafase),length(lineafase)+1:2*length(lineafase))=rot90(mapafase1c,1);
mapafase(length(lineafase)+1:2*length(lineafase),1:length(lineafase))=rot90(mapafase1c,3);
mapafase(length(lineafase)+1:2*length(lineafase),length(lineafase)+1:2*length(lineafase))=mapafase1c;


