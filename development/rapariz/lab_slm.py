# !/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import os
import time

import cv2
import screeninfo
import serial
import serial.tools.list_ports
from scipy import ndimage

import py_slm.camera.tisgrabber as IC
from Ejercicios_TFG import Ejercicios
from diffractio import degrees, mm, nm, np, plt, sp, um
from diffractio.scalar_masks_XY import Scalar_mask_XY
from diffractio.utils_common import nearest
from diffractio.utils_optics import field_parameters
from py_slm.smc100 import SMC100

s = 1.

# Configuration Params for Holoeye (SLM)
CONF_HOLOEYE2500 = {}
CONF_HOLOEYE2500['num_pixels'] = (1024, 768)
CONF_HOLOEYE2500['size_pixels'] = (19 * um, 19 * um)
CONF_HOLOEYE2500['wavelength'] = 632.8 * nm
CONF_HOLOEYE2500['callibration_table'] = None
CONF_HOLOEYE2500['astigmatism_removal'] = None
CONF_HOLOEYE2500['pos_screen'] = None

# Configuration Params for Screen at lab
CONF_PACKARD_BELL = {}
CONF_PACKARD_BELL['num_pixels'] = (1440, 900)
CONF_PACKARD_BELL['size_pixels'] = (285 * um, 295 * um)
CONF_PACKARD_BELL['wavelength'] = 632.8 * nm
CONF_PACKARD_BELL['callibration_table'] = None
CONF_PACKARD_BELL['astigmatism_removal'] = None
CONF_PACKARD_BELL['pos_screen'] = None

CONF_IMAGING_SOURCE = {}
CONF_IMAGING_SOURCE['id_camera'] = "DMx 72BUC02 14210296"
CONF_IMAGING_SOURCE['size_pixels'] = (2.2 * um, 2.2 * um)
CONF_IMAGING_SOURCE['format'] = "Y800 (1024x768)"
CONF_IMAGING_SOURCE['framerate'] = 5

ID_SCREEN = 1

DIR_DRIVER_CAMERA = "C:/Users/luismiguel/bitbucket/py_slm/py_slm/camera"


def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print("Error: Creating directory." + directory)


def desviation_image(image=None, display=False):
    ddepth = cv2.CV_16S
    kernel_size = 3

    # Imagen = cv2.GaussianBlur(image, (3, 3), 0)
    Imagen_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    dst = cv2.Laplacian(Imagen_gray, ddepth, kernel_size)
    abs_dst = cv2.convertScaleAbs(dst)
    std_d = np.std(abs_dst)

    if display:
        cv2.imshow(window_name, abs_dst)
        cv2.waitKey(0)

    return std_d


def variance_image(image):
    return cv2.Laplacian(image, cv2.CV_64F).var()


class lab_SLM(object):
    """
    Esta clase esta programada para manejar el SLM del laboratorio de optica de
    la UCM. A continuacion hay un ejemplo de como usar la clase.

    Ejemplo:

    slm = lab_slm.lab_SLM()
    t1 = Scalar_mask_XY(slm.x0, slm.y0, slm.wavelength)
    t1.ring(r0 = (0 * um, 0 * um), radius1 = (60 * um, 60 * um),
            radius2 = (30 * um, 30 * um), angle = 0 * degrees)

    slm.mask_to_rawImage(mask_XY = t1, kind = 'intensity', normalize = True)
    slm.send_image_screen(verbose = True)
    slm.acquire_image(draw=True, remove_background=False, is_closed=False)

    slm.all_closed(verbose = True)

    """

    def __init__(self,
                 config_SLM=CONF_HOLOEYE2500,
                 config_Camera=CONF_IMAGING_SOURCE,
                 inicio=True,
                 range=(110 * mm, 120 * mm),
                 pos=10,
                 veloc=10 * mm / s):
        """
        Se configuran todos los parametros de la clase a partir de los parametros
        del SLM y la camara. Estos parametros estan en los diccionarios definidos
        anteriormente.

        Ademas, el codigo inicial debe iniciar una prueba de inicio con la imagen
        de la UCM, ucm.jpg, confirmando que todo funciona correctamente.
        """
        self.config_SLM = config_SLM
        self.config_Camera = config_Camera
        self.wavelength = config_SLM['wavelength']
        self.num_pixels = config_SLM['num_pixels']
        pixel_size = config_SLM['size_pixels']

        self.x0 = np.linspace(-pixel_size[0] * self.num_pixels[0] / 2,
                              pixel_size[0] * self.num_pixels[0] / 2,
                              self.num_pixels[0])

        self.y0 = np.linspace(-pixel_size[1] * self.num_pixels[1] / 2,
                              pixel_size[1] * self.num_pixels[1] / 2,
                              self.num_pixels[1])

        os.chdir('C:/Users/luismiguel/bitbucket/py_slm/docs/source/TFG_rapariz')

        self.id_screen = ID_SCREEN
        self.camera = None
        # self.motor.close()
        self.motor = None
        self.background = None
        self.image_raw = None
        self.mask = None
        self.IDimage = None
        self.focal_position = None
        self.scale_factor_pixel = None
        self.scale_factor_um = None
        self._init_camera()
        self.get_background()
        self._init_motor(vel=veloc)
        if inicio:
            self._init_mask()
            self.focalizar_mask(default=True, pics=pos, rango=range,
                                graph=True, nameFolder='Resultados_calibracion')
            self.acquire_image(
                draw=True,
                remove_background=False,
                filename='ucm_image.jpg',
                is_closed=False)

        print("Todo Ok...")

    def _init_camera(self):
        """
        Inicializacion de la camara que se va a utilizar en el Laboratorio.
        """
        camera1 = IC.TIS_CAM()
        camera1.open(self.config_Camera['id_camera'])
        camera1.SetVideoFormat(self.config_Camera['format'])
        camera1.SetFrameRate(self.config_Camera['framerate'])
        camera1.StartLive()

        self.camera = camera1

        print("Camara Ok...")

    def _init_motor(self, vel=10 * mm / s):
        """
        Encendemos el motor para desplazar la camara.
        """
        ports = list(serial.tools.list_ports.comports())
        for port in ports:
            print port

        for p in ports:
            if 'COM3' in p.description:
                print(p)
                # Connection to port
                ser = serial.Serial(p.device)
                ser.close()

        def test_configure():
            smc100 = SMC100(1, 'COM3', silent=True)
            smc100.reset_and_configure()
            # make sure there are no errors
            assert smc100.get_status()[0] == 0
            del smc100

        test_configure()

        motor = SMC100(1, 'COM3', silent=True)

        self.motor = motor

        self.motor.home()

        self.position = self.motor.get_position(verbose=True)
        self.motor.set_velocity(vel)
        self.velocity = self.motor.get_velocity(verbose=True)

        print("Motor Ok...")

    def _init_mask(self):
        """
        Enviamos una imagen inicial, el logo de la UCM guardado en un fichero JPG,
        a la SLM

        """
        ucm = cv2.imread('logo_ucm.png', cv2.IMREAD_UNCHANGED)
        # ucm = ndimage.rotate(ucm, 180)
        ucm = cv2.flip(ucm, -1)
        ucm = cv2.flip(ucm, 1)

        self.image_raw = ucm
        if ucm.max() <= 1. and ucm.min() >= 0.:
            self.condition_image(normalize=True)
        self.send_image_screen()

        print("Mascara Ok...")

    def move_motor(self, z=0 * mm, kind='abs'):
        """
        This module moves the motor to the specified distance.

        Args:
            z (float): Distance at which the motor will be moved
            kind (str): Type of movement.
                Options:
                    - 'abs': Moves the camera to a distance z from the origin
                    - 'rel': Moves the camera a distance z from its actual
                            position
                    - 'home': Moves the camera home

        Example:
            move_motor(z = 100 * mm, kind = 'abs)
        """

        if kind == 'abs':
            self.motor.move_absolute(z)
        elif kind == 'rel':
            self.motor.move_relative(z)
        elif kind == 'home':
            self.motor.home()

        self.position = self.motor.get_position(verbose=True)

    def vel_motor(self, vel=None, kind='set'):
        """
        This module changes or displays the velocity of the motor

        Args:
            vel (float): Velocity at which the motor will be moving
            kind (str): Type of movement.
                Options:
                    - 'set': Changes the velocity to the specified in 'vel'. If
                            a velocity is not given, the default velocity will
                            be 10 mm/s
                    - 'get': Displays the velocity at which the motor is moving

        Example:
            vel_motor(vel = 10 * mm/s, kind = 'set')
        """
        assert (vel > 0 or vel == None)

        if kind == 'set':
            if vel == None:
                # Default velocity
                self.motor.set_velocity(10 * mm / s)
            else:
                self.motor.set_velocity(vel)
        elif kind == 'get':
            self.motor.get_velocity()

        self.velocity = self.motor.get_velocity(verbose=True)

    def acquire_image(self,
                      draw=True,
                      remove_background=True,
                      filename='',
                      is_closed=False):
        """
        Gets an image from the camera1
        """
        self.camera.SnapImage()
        image = self.camera.GetImage()  # Get the image

        if remove_background is True:
            image = image - self.background
            image[image < 0] = 0
        if draw is True:
            self.show_image(image)
        if filename != '':
            if draw is False:
                self.show_image(image)
            plt.savefig(filename)
        if is_closed is True:
            plt.close()

        self.camera.StartLive()

        return image

    def mask_to_rawImage(self, mask_XY=None, kind='amplitude',
                         normalize=False):
        """Gets a XY mask, conditions it:

        """
        if mask_XY != None:
            self.mask = mask_XY

        amplitude, intensity, phase = field_parameters(self.mask.u)

        if kind == 'amplitude':
            image_raw = amplitude
        elif kind == 'intensity':
            image_raw = intensity
        elif kind == 'phase':
            phase = (phase + np.pi) % (2 * np.pi) - np.pi

            image_raw = phase + np.pi
            # image_raw[0, 0] = 0
            # image_raw[0, 1] = 2 * np.pi
            image_raw = image_raw / (2 * np.pi)

        self.image_raw = image_raw
        self.condition_image(normalize=normalize)

        return self.image_raw

    def condition_image(self, normalize=False):

        if normalize is True:
            self.image_raw = (
                255 * self.image_raw / self.image_raw.max()).astype(np.uint8)
        else:
            self.image_raw = (self.image_raw).astype(np.uint8)

    def get_background(self, level_1=1):
        """Get an image with the SLM at 0
        This image is used as background and substracted
        TODO: Hay que ver en qué condiciones se envía la imagen
        """
        cambio = False
        if np.any(self.image_raw) != None:
            last_imageraw = self.image_raw
            cambio = True

        # swith modulator off
        t1 = Scalar_mask_XY(x=self.x0, y=self.y0, wavelength=self.wavelength)
        t1.one_level(level=level_1)
        self.mask_to_rawImage(mask_XY=t1, kind='intensity', normalize=True)
        self.send_image_screen()
        # Get image at a certain camera conditions
        cv2.waitKey(500)
        self.camera.SnapImage()

        # Get the image
        self.background = self.camera.GetImage()

        cv2.destroyAllWindows()

        # Reponemos la anterior máscara, si la había
        if cambio:
            self.image_raw = last_imageraw
            self.send_image_screen(id_screen=1, verbose=True)

    def show_background(self):
        self.show_image(self.background)

    def send_image_screen(self, id_screen='', verbose=False):
        """
        takes the images and sends the images to a screen in full size,
        according to  ***TODO

        TODO: make dynamic use of image_screen as proposed in jupyter notebook

        El cv2.waitKey() lo introducimos para que el SLM tenga tiempo de adoptar la
        forma de la imagen o mascara a introducir.
        """

        image = self.image_raw
        if verbose is True:
            print("send_image_screen: max={}. min={}".format(
                image.max(), image.min()))
            self.show_image(image)
        if id_screen in ('', None, []):
            ids = self.id_screen
        else:
            ids = id_screen

        screen = screeninfo.get_monitors()[ids]
        window_name = 'SLM projector'
        cv2.namedWindow(window_name, cv2.WND_PROP_FULLSCREEN)
        cv2.moveWindow(window_name, screen.x - 1, screen.y - 1)
        cv2.setWindowProperty(window_name, cv2.WND_PROP_FULLSCREEN,
                              cv2.WINDOW_FULLSCREEN)
        cv2.imshow(window_name, image)
        cv2.waitKey(2500)

    def show_image(self, image, verbose=True):
        IDimage = plt.imshow(
            image, interpolation='bilinear', aspect='auto', origin='lower')
        plt.axis('off')
        plt.axis('equal')
        IDimage.set_cmap("gray")
        if verbose:
            print("max={}. min={}".format(image.max(), image.min()))
            print("shape = {}".format(image.shape))

        self.IDimage = IDimage
        return self.IDimage

    def focalizar_mask(self,
                       default=True,
                       rango=(110 * mm, 120 * mm),
                       pics=10,
                       graph=False,
                       nameFolder='Imagenes_focalizar_mascara',
                       verbose=False):
        """
        This module focalizes the image of a mask within the range specified.

        number of pictures ('pics') will be taken in order to select the best
        position.

        Args:
            rango (float, float): Range of z at which the motor will be moved
            pics (int): Number of pictures between the initial and the last
                        position
            nameFolder(str): Name of the folder to be created
            verbose (bool): if True, a text will be displayed with the new
                            distance. False is the default option

        Example:
            focalizar_mask(rango = (110 * mm, 120 * mm),
                            pics = 30,
                            verbose = False)
        """
        if default:
            # Guardamos la anterior mascara para enviarselo de vuelta al SLM
            last_mask = self.image_raw

            # mascara = Scalar_mask_XY(self.x0, self.y0, self.wavelength)
            mascara = Ejercicios(self.x0, self.y0, self.wavelength)
            # mascara.grating_2D_ajedrez(
            #    period=2 * mm, amin=0., amax=1., fill_factor=0.5)
            # mascara.ring(r0 = (0 * um, 0 * um), radius1 = (1000 * um, 1000 * um), radius2 = (700 * um, 700 * um))
            mascara.hiper_ellipse(
                r0=(0 * um, 0 * um), radius=(2. * mm, .75 * mm), angle=0 * degrees, n=0.5)

            self.mask = mascara
            self.mask_to_rawImage(
                mask_XY=None, kind='amplitude', normalize=True)
            self.send_image_screen(id_screen=1)

        varianza = np.array([])
        std_deviation = np.array([])
        zi, zf = rango
        stops = np.linspace(zi, zf, pics)
        directorio = 'C:/Users/luismiguel/bitbucket/py_slm/docs/source/TFG_rapariz' + nameFolder
        createFolder(directorio)
        # Hay que meterle el directorio completo
        os.chdir(directorio)

        for i, stop in enumerate(stops):
            self.motor.move_absolute(stop)
            if i == 0:
                raw_input(
                    'Pulse enter cuando haya posicionado el polarizador a su gusto.')
            self.acquire_image(
                draw=False,
                remove_background=False,
                filename='image_mask{}.jpg'.format(i),
                is_closed=True)
            imagen = cv2.imread('image_mask{}.jpg'.format(i))
            gray_img = cv2.cvtColor(imagen, cv2.COLOR_BGR2GRAY)
            varianza = np.append(varianza, variance_image(gray_img))
            # std_deviation = np.append(std_deviation, desviation_image(imagen))
            self.camera.StartLive()

        os.chdir('C:/Users/luismiguel/bitbucket/py_slm/docs/source/TFG_rapariz')

        # focal_zpoint_mask = round(stops[varianza == max(varianza)], 1)
        # medio = (max(varianza)+min(varianza))/2
        medio = np.mean(varianza)
        ii, valor, dist = nearest(varianza, medio)
        focal_zpoint_mask = round(stops[ii], 3)
        self.motor.move_absolute(focal_zpoint_mask)

        if graph:
            fig = plt.figure()
            ax1 = fig.add_subplot(111)
            # stops_mm = stops/1000
            ax1.plot(stops, varianza)
            punto = ax1.plot([stops[ii]], [valor], 'ro')
            separ = np.abs(stops[0] - stops[1])
            nota = plt.annotate('(z, varianza) = ({}, {})'.format(round(focal_zpoint_mask, 1), round(varianza[ii], 1)),
                                xy=(focal_zpoint_mask,
                                    varianza[ii]), xycoords='data',
                                xytext=(stops[0] + 2 * separ, 1.05 * varianza[ii]), fontsize=9,
                                arrowprops=dict(arrowstyle="->", connectionstyle="Arc3, rad=0.2"))
            plt.grid()
            ax1.set_xlim(zi, zf)
            ax1.set_xlabel('z (um)')
            ax1.set_ylabel('var')
            ax1.set_title('Varianza vs. z')

            plt.savefig('varianza.JPG')
            plt.show()

        if verbose:
            print("La distancia para focalizar la imagen es en z = %.2f" %
                  (focal_zpoint_mask))

        if default:
            self.image_raw = last_mask
            self.send_image_screen(id_screen=1, verbose=False)

        self.focal_position = focal_zpoint_mask
        self.position = self.motor.get_position()

    def focalizar_lens(self,
                       rango=(110 * mm, 120 * mm),
                       focal_l=10 * mm,
                       radii=20 * mm,
                       pics=20,
                       nameFolder='Imagenes_focalizar_lente',
                       verbose=True):
        """
        This module creates a lens and focalizes the image within the range
        specified. A number of pictures ('pics') will be taken in order to
        select the best position.

        Args:
            rango (float, float): Range of z at which the motor will be moved
            focal_l (float): Focal distance of the lens-mask
            pics (int): Number of pictures between the initial and the last
                        position
            radii (float, float): The radius of the lens-mask
            verbose (bool): if True, a text will be displayed with the new distance.
                            False is the default option

        Example:
            focalizar_lens(rango = (110 * mm, 120 * mm),
                           pics = 30, verbose = False)
        """
        if isinstance(radii, (float, int)):
            radii = (radii, radii)
        max_intensities = np.array([])
        mascara = Scalar_mask_XY(self.x0, self.y0, self.wavelength)
        mascara.lens(
            r0=(0 * mm, 0 * mm),
            radius=radii,
            focal=focal_l,
            angle=0 * degrees,
            mask=True)
        zi, zf = rango
        stops = np.linspace(zi, zf, pics)
        directorio = 'C:/Users/luismiguel/bitbucket/py_slm/docs/source/TFG_rapariz' + nameFolder
        createFolder(directorio)
        # Hay que meterle el directorio completo
        os.chdir(directorio)

        for i, stop in enumerate(stops):
            self.motor.move_absolute(stop)
            self.acquire_image(
                draw=False,
                remove_background=False,
                filename='image_lens{}.jpeg'.format(i),
                is_closed=True)
            imagen = cv2.imread('image_lens{}.jpeg'.format(i))
            np.append(max_intensities, imagen.max())

        self.mask = mascara
        self.mask_to_rawImage(kind='intensity', normalize=True)
        self.send_image_screen(id_screen=1, verbose=False)

        os.chdir('C:/Users/luismiguel/bitbucket/py_slm/docs/source/TFG_rapariz')

        focal_zpoint_lens = round(
            stops[max_intensities == max(max_intensities)], 1)
        self.motor.move_absolute(focal_zpoint_lens)

        if verbose:
            print("La distancia para focalizar la lente es en z = %.2f" %
                  (focal_zpoint_lens))
        self.focal_position = focal_zpoint_lens
        self.position = self.motor.get_position()

    def chess_scale_factor(self,
                           periodo=1 * mm,
                           z='z_focal',
                           f_factor=0.5,
                           rtn='um'):

        mascara = Scalar_mask_XY(self.x0, self.y0, self.wavelength)
        mascara.grating_2D_ajedrez(
            period=periodo, amin=0., amax=1., fill_factor=f_factor)

        self.mask = mascara
        self.mask_to_rawImage(kind='amplitude', normalize=True)
        self.send_image_screen(verbose=True)

        if z == 'z_focal':
            self.motor.move_absolute(self.focal_position)
        else:
            self.motor.move_absolute(z)

        self.acquire_image(
            draw=False,
            remove_background=False,
            filename='chess_calibration.jpg',
            is_closed=False)
        chess_image = cv2.imread('chess_calibration.jpg')
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.imshow(chess_image)
        puntos_camara = fig.ginput(2, timeout=0, show_clicks=True)
        # cv2.waitKey(0)
        plt.close(fig)
        n2 = int(input('¿Cuantos bloques has escogido?  '))
        dist_camara_P = np.sqrt(
            (puntos_camara[1][0] - puntos_camara[0][0])**2
            + (puntos_camara[1][1] - puntos_camara[0][1])**2) / n2

        dist_camara_um = dist_camara_P * self.config_Camera['size_pixels'][0]
        scale_factor_um = periodo / dist_camara_um

        scale_factor_pixel = scale_factor_um * \
            (self.config_Camera['size_pixels']
             [0] / self.config_SLM['size_pixels'][0])

        self.scale_factor_pixel = scale_factor_pixel
        self.scale_factor_um = scale_factor_um
        self.position = self.motor.get_position()

        if rtn == 'pixel':
            return self.scale_factor_pixel
        elif rtn == 'um':
            return self.scale_factor_um

    def square_scale_factor(self, stop=115 * mm, filename0='', r0=(0 * um, 0 * um), lado=(1 * mm, 1 * mm)):

        if isinstance(lado, (float, int)):
            lado = (lado, lado)

        mascara = Scalar_mask_XY(self.x0, self.y0, self.wavelength)
        mascara.square(r0=r0, size=lado, angle=0 * degrees)
        self.mask = mascara

        self.mask_to_rawImage(mask_XY=None, kind='amplitude', normalize=True)
        self.send_image_screen(id_screen=1, verbose=True)
        self.move_motor(z=stop)
        raw_input('Coloque el modulador a su gusto.')

        square_image = self.acquire_image(draw=True,
                                          filename=filename0,
                                          remove_background=False,
                                          is_closed=False)

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.imshow(square_image)
        puntos_camara = fig.ginput(2, timeout=0, show_clicks=True)

        dist_camara_p = np.sqrt(
            (puntos_camara[1][0] - puntos_camara[0][0])**2 + (puntos_camara[1][1] - puntos_camara[0][1])**2)
        dist_camara_um = dist_camara_p * self.config_Camera['size_pixels'][0]
        square_scale_factor = lado[0] / dist_camara_um

        ax.set_title('Factor de escala = {}'.format(
            round(square_scale_factor, 3)))
        fig.savefig('square_factor_scale_1mm.JPG')
        plt.close(fig)

        self.square_scale_factor = square_scale_factor

        return square_scale_factor

    def fotos_rango(self, nameFolder, rango=(100 * mm, 125 * mm), pics=25, velocidad=20 * mm / s):
        directorio = 'C:/Users/luismiguel/bitbucket/py_slm/docs/source/TFG_rapariz/' + nameFolder
        # Creamos una carpeta donde guardaremos las imágenes del código
        createFolder(directorio)
        os.chdir(directorio)  # Nos movemos al nuevo directorio

        zi, zf = rango
        stops = np.linspace(zi, zf, pics)
        self.move_motor(kind='home')
        self.vel_motor(vel=velocidad, kind='set')

        for i, stop in enumerate(stops):
            self.move_motor(stop)
            self.acquire_image(draw=False,
                               filename='imagen_{}.jpg'.format(i),
                               remove_background=False,
                               is_closed=True)

        os.chdir('C:/Users/luismiguel/bitbucket/py_slm/docs/source/TFG_rapariz')

        return

    def haz_lente(self, range, r0, nameFolder, focal, radius, percentage, num_stops, graph, verbose):
        """
        To Do: Hay que acabar el modulo, está incompleto.

            Esta funcion va a tomar fotos dentro del rango dado para recrear el haz
            del laser tras atravesar una lente convergente y ver como se concentra
            en las proximidades de la distancia focal de la lente y luego se vuelve
            a dispersar.

            Args:

                range (float, float):
                r0 (float, float):
                focal (float):
                radius(float, float):
                percentage (float):
                num_pics (int):
                graph (bool):
                verbose (bool):
        """
        # Primero tomamos las fotos para luego eliminar background
        zi = range[0]
        zf = range[1]
        max_intensities = []
        stops = np.linspace(zi, zf, num_stops)

        # Creamos la máscara para enviarla al SLM
        mascara = Scalar_mask_XY(self.x0, self.y0, self.wavelength)
        mascara.lens(r0=r0, radius=radius, focal=focal,
                     angle=0 * degrees, mask=True)
        self.mask = mascara

        self.mask_to_rawImage(mask=None, kind='intensity', normalize=True)
        self.send_image_screen(id_screen=1, verbose=True)

        # Creamos una carpeta para las imagenes del módulo
        directorio = 'C:/Users/luismiguel/bitbucket/py_slm/docs/source/TFG_rapariz' + nameFolder
        self.createFolder(directorio)

        # Se introduce el directorio completo de la carpeta creada
        os.chdir(directorio)

        for i1, stop in enumerate(stops):
            self.motor.move_absolute(stop)
            self.acquire_image(draw=False,
                               remove_background=False,
                               filename='lens_image_{}.JPG',
                               is_closed=True)

            img = cv2.imread('lens_image_{}.JPG'.format(i1))
            max_intensities.append(img.max())
            img = img[img < (percentage * img.max())]
            size1 = config_SLM['num_pixels'][0] * config_SLM['size_pixels'][0]
            fig = plt.figure()
            ax = fig.add_subplot(111)
            ax.get_xaxis().set_visibility(False)
            ax.get_yaxis().set_visibility(False)
            ax.imshow(img)
            fig.savefig('lens_image_{}.jpg'.format(i1))
            plt.close(fig)

        os.chdir('C:/Users/luismiguel/bitbucket/py_slm/docs/source/TFG_rapariz')

    def focalizar_mask_1(self,
                         default=True,
                         rango=(110 * mm, 120 * mm),
                         pics=10,
                         graph=False,
                         nameFolder='Imagenes_focalizar_mascara',
                         verbose=False):
        """
        This module focalizes the image of a mask within the range specified.

        number of pictures ('pics') will be taken in order to select the best
        position.

        Args:
            rango (float, float): Range of z at which the motor will be moved
            pics (int): Number of pictures between the initial and the last
                        position
            nameFolder(str): Name of the folder to be created
            verbose (bool): if True, a text will be displayed with the new
                            distance. False is the default option

        Example:
            focalizar_mask(rango = (110 * mm, 120 * mm),
                            pics = 30,
                            verbose = False)
        """
        if default:
            # Guardamos la anterior mascara para enviarselo de vuelta al SLM
            last_mask = self.image_raw

            mascara = Scalar_mask_XY(self.x0, self.y0, self.wavelength)
            mascara.lens(r0=(0 * um, 0 * um), radius=(20 * um,
                                                      20 * um), focal=(10 * mm, 10 * mm))
            # mascara.ring(r0 = (0 * um, 0 * um), radius1 = (1000 * um, 1000 * um), radius2 = (700 * um, 700 * um))

            self.mask = mascara
            self.mask_to_rawImage(mask_XY=None, kind='phase', normalize=True)
            self.send_image_screen(id_screen=1)

        std_deviation = np.array([])
        zi, zf = rango
        stops = np.linspace(zi, zf, pics)
        directorio = 'C:/Users/luismiguel/bitbucket/py_slm/py_slm/calibrations/' + nameFolder
        createFolder(directorio)
        # Hay que meterle el directorio completo
        os.chdir(directorio)

        for i, stop in enumerate(stops):
            self.motor.move_absolute(stop)
            if i == 0:
                raw_input(
                    'Pulse enter cuando haya posicionado el polarizador a su gusto.')
            self.acquire_image(
                draw=False,
                remove_background=True,
                filename='image_mask{}.jpg'.format(i),
                is_closed=True)
            imagen = cv2.imread('image_mask{}.jpg'.format(i))
            # gray_img = cv2.cvtColor(imagen, cv2.COLOR_BGR2GRAY)
            std_deviation = np.append(std_deviation, np.std(imagen))
            # self.camera.StartLive()

        os.chdir('C:/Users/luismiguel/bitbucket/py_slm/py_slm/calibrations')

        # focal_zpoint_mask = round(stops[varianza == max(varianza)], 1)
        # medio = (max(varianza)+min(varianza))/2
        # medio = np.mean(varianza)
        # ii, valor, dist = nearest(varianza, medio)
        focal_zpoint_mask = round(
            stops[std_deviation == min(std_deviation)], 3)
        self.motor.move_absolute(focal_zpoint_mask)

        if graph:
            fig = plt.figure()
            ax1 = fig.add_subplot(111)
            # stops_mm = stops/1000
            ax1.plot(stops, std_deviation)
            punto = ax1.plot([focal_zpoint_mask], [min(std_deviation)], 'ro')
            separ = np.abs(stops[0] - stops[1])
            nota = plt.annotate('(z, desv. estandar) = ({}, {})'.format(round(focal_zpoint_mask, 1), round(min(std_deviation), 1)),
                                xy=(focal_zpoint_mask,
                                    min(std_deviation)), xycoords='data',
                                xytext=(stops[0] + 2 * separ, 1.05 * min(std_deviation)), fontsize=9,
                                arrowprops=dict(arrowstyle="->", connectionstyle="Arc3, rad=0.2"))
            plt.grid()
            ax1.set_xlim(zi, zf)
            ax1.set_xlabel('z (um)')
            ax1.set_ylabel('var')
            ax1.set_title('Desviacion Estandar vs. z')
            plt.show()

        if verbose:
            print("La distancia para focalizar la imagen es en z = %.2f" %
                  (focal_zpoint_mask))

        if default:
            self.image_raw = last_mask
            self.send_image_screen(id_screen=1, verbose=False)

        self.focal_position = focal_zpoint_mask
        self.position = self.motor.get_position()

    def __del__(self):
        """
        Este módulo está pensado para que se cierre el motor siempre que se deja de usar el módulo
        """
        self.motor.set_velocity(20 * mm / s)
        self.motor.home()
        cv2.destroyAllWindows()

        self.motor.close()
        self.camera.StopLive()

        self.camera = None
        self.motor = None
        self.background = None
        self.image_raw = None
        self.mask = None
        self.IDimage = None
        self.scale_factor_pixel = None
        self.scale_factor_um = None
        print('Todo se ha cerrado correctamente.')
