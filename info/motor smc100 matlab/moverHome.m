function moverHome(obj_serial)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% F. J. Salgado-Remacha
% 04/10/10
% moverHome(obj_serial)
% busca HOME
% Hasta que no alcanza el destino no sale del programa
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

homeSearch(obj_serial)
%% compruebo que el movimiento ha acabado
posActual=(getCurrentPosition(obj_serial));
destino=0;    
    
    while abs(posActual-destino)>=0.001
       pause(0.1);
       posActual=(getCurrentPosition(obj_serial));
       
    end