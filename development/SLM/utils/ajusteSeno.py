# -*- coding: utf-8 -*-
"""
Created on Sun Oct  7 07:42:54 2012

@author: luismiguel
"""

from scipy.optimize import leastsq

from rutas import *

#genero la función
datosExperimentales = data1D(
    xtext=r"$x\,(mm)$", ytext=r"$y\,(mm)$", title="grafica Seno")
datosExperimentales.senal_regular(
    xlim=(0, 1),
    num_data=500,
    func='.5+0.3*sp.sin(2*sp.pi*(self.x-0.5))+0.1*sp.sin(2*sp.pi*self.x*5)')
datosExperimentales.add_noise(sc=(0.025, ))
x = datosExperimentales.x
yexp = datosExperimentales.y


def error(p, y, x):
    A, B, x0, period = p
    return (y - (A * sp.sin(2 * sp.pi * (x - x0) / period) + B))**2


"""ajuste propiamente dicho"""
plsq, cov_x = leastsq(
    error, [1.0, 1.0, 1.0, 1.0],
    args=(datosExperimentales.y, datosExperimentales.x))
#plsq (array([ 5.11878922, 1.14629313]), 'Both actual and predicted relative reductions in the sum of squares\n are at most 0.000000')
print plsq

"datos del ajuste"
A0 = plsq[0]
B0 = plsq[1]
x0 = plsq[2]
period = plsq[3]
print A0, B0, x0, period

y_ajustado = A0 * sp.sin(2 * sp.pi * (x - x0) / period) + B0
r = yexp - y_ajustado

residuos = data1D(xtext=r"$x\,(mm)$", ytext=r"$y\,(mm)$", title="residuos")
residuos.generarSenal(x, r)

"draw datos"
datosExperimentales.draw()
plt.hold(True)
plt.plot(x, y_ajustado, 'r', linewidth=2)

"draw residuos"
residuos.draw()

plt.show()
