#  !/usr/bin/env python3
#  -*- coding: utf-8 -*-
#  ------------------------------------
#  Autor:    Luis Miguel Sanchez Brea
#  Fecha     2012/11/01 (version 1.0)
#  Licencia: copyright
#  Objetivo: examples para la clase LCD
#  -------------------------------------

import numpy as np
import scipy as sp
import LCD
from LCD import *


def draw_beta_delta():
    # carga un LCD y hace las simulaciones
    LCD0 = LCD(modelo=modeloKopin)
    LCD0.draw(kind='beta', title='modelo kopin')
    LCD0.draw(kind='delta', title='modelo kopin')
    LCD0.draw(kind='beta_delta', title='modelo kopin')


def test_modeloLM1():
    LCD0 = LCD(modeloLM1)
    title = 'test_modeloLM1'
    LCD0.draw(kind='u_escalar', modelo='DC', tecnica='matrix', title=title)
    LCD0.draw(
        kind='barrido_intesities', modelo='DC', tecnica='matrix', title=title)
    LCD0.draw(kind='u_real_imag', modelo='DC', tecnica='matrix', title=title)


def test_modeloBurmanFase1():
    LCD0 = LCD(modeloBurmanFase1)
    title = 'burman phase 1'
    LCD0.draw(kind='u_escalar', modelo='DC', tecnica='matrix', title=title)
    LCD0.draw(
        kind='barrido_intesities', modelo='DC', tecnica='matrix', title=title)
    LCD0.draw(kind='u_real_imag', modelo='DC', tecnica='matrix', title=title)
    LCD0.draw(
        kind='beta_delta',
        modelo='DC',
        tecnica='matrix',
        title='',
        theta1='',
        theta2='')


def test_modeloBurmanFase2():
    LCD0 = LCD(modeloBurmanFase2)
    title = 'burman phase 2'
    LCD0.draw(kind='u_escalar', modelo='DC', tecnica='matrix', title=title)
    LCD0.draw(
        kind='barrido_intesities', modelo='DC', tecnica='matrix', title=title)
    LCD0.draw(kind='u_real_imag', modelo='DC', tecnica='matrix', title=title)


def test_modeloBurmanAmplitud():
    LCD0 = LCD(modeloBurmanAmplitud)
    title = 'burman amplitude'
    LCD0.draw(kind='u_escalar', modelo='DC', tecnica='matrix', title=title)
    LCD0.draw(
        kind='barrido_intesities', modelo='DC', tecnica='matrix', title=title)
    LCD0.draw(kind='u_real_imag', modelo='DC', tecnica='matrix', title=title)


def test_modeloBurmanBinario():
    LCD0 = LCD(modeloBurmanBinario)
    title = 'burman binario'
    LCD0.draw(kind='u_escalar', modelo='DC', tecnica='matrix', title=title)
    LCD0.draw(
        kind='barrido_intesities', modelo='DC', tecnica='matrix', title=title)
    LCD0.draw(kind='u_real_imag', modelo='DC', tecnica='matrix', title=title)


def test_modelo_experimental_sony():
    LCD0 = LCD(modelo_experimental_sony)
    title = 'modelosony'
    LCD0.draw(kind='u_escalar', modelo='DC', tecnica='matrix', title=title)
    LCD0.draw(
        kind='barrido_intesities', modelo='DC', tecnica='matrix', title=title)
    LCD0.draw(kind='u_real_imag', modelo='DC', tecnica='matrix', title=title)


def test_DS():
    LCD0 = LCD(modelo=modeloBurmanFase1)
    LCD0.gl_actual = 128

    ds1 = LCD0.DS(tecnica='directa')
    print ds1

    ds2 = LCD0.DS(tecnica='matrix')
    print ds2

    print "diferencias", ds2 - ds1


def test_DC():
    LCD0 = LCD(modelo=modeloBurmanFase1)
    LCD0.gl_actual = 128

    dc1 = LCD0.DC(tecnica='directa')
    print dc1

    dc2 = LCD0.DC(tecnica='matrix')
    print dc2

    print "diferencias", dc2 - dc1


def test_drawings():
    LCD0 = LCD(modelo=modeloBurmanFase1)
    LCD0.draw(kind='u_escalar', modelo='DC', tecnica='matrix')
    LCD0.draw(kind='u_real_imag', modelo='DC', tecnica='matrix')
    LCD0.draw(kind='barrido_intesities', modelo='DC', tecnica='matrix')


def test_fields_salida():
    LCD0 = LCD(modelo=modeloBurmanFase1)

    # imprime la transmitancia
    u_escalar, ux, uy = LCD0.fields_salida(modelo='DC', tecnica='directa')
    print "u_escalar: ", u_escalar
    print "ux: ", ux
    print "uy: ", uy

    # se consigue el field en ampitud y phase
    amplitude, phase = LCD0.get_fields(
        kind='u_amplitude_phase', modelo='DC', tecnica='matrix')

    # se consigue el field completo
    u_escalar = LCD0.get_fields(
        kind='u_escalar', modelo='DC', tecnica='matrix')

    # no se consigue, pero se dibuja
    LCD0.draw(kind='u_escalar', modelo='DC', tecnica='matrix')

    # se dibuja para comparar
    plt.figure()
    plt.subplot(1, 2, 1)
    plt.plot(LCD0.gl, amplitude, 'k')
    plt.ylim(ymin=0, ymax=max(1, max(amplitude)))
    plt.xlim(xmin=LCD0.gl[0], xmax=LCD0.gl[-1])
    plt.title('amplitude')

    plt.subplot(1, 2, 2)
    plt.plot(LCD0.gl, (phase * 180 / np.pi) % 360, 'k')
    plt.xlim(xmin=LCD0.gl[0], xmax=LCD0.gl[-1])
    plt.ylim(ymin=0)
    plt.title('phase')


def test_matrices():
    LCD0 = LCD(modelo=modeloBurmanFase1)
    matrices = LCD0.get_matrices(modelo='DC', tecnica='directa')
    print matrices.shape

    print "para el level 0 resulta: "
    level0 = np.matrix(np.squeeze(matrices[:, :, 0]))
    print level0

    print "si se ilumina con polarization lineal 1,0"
    plane_wave = luzLineal(alfa=0)
    print plane_wave

    haz_salida = level0 * plane_wave
    print haz_salida


def test_intesidadSistema():
    # se carga el modelo
    LCD0 = LCD(modelo=modeloLM1)
    # configuración inicial
    gl = np.linspace(0, 255, 256)
    # matrix de intesities
    intesities = np.zeros(gl.shape)
    for i, gl_actual in zip(range(len(gl)), gl):
        # se carga el level de gris
        LCD0.gl_actual = gl_actual
        # se calcula la intensity
        intesities[i] = LCD0.intensity_salida(modelo='DS', tecnica='matrix')

    ints = data1D(
        x=LCD0.gl,
        y=intesities,
        xtext=r"$g.l.$",
        ytext=r"$intensity(gl)$",
        title="modeloBurmanAmplitud")
    ints.draw()
    plt.ylim(ymin=0, ymax=1)


def test_barridoPolarizersintensity1():
    """
    test donde se carga un level de gris y se obtiene la intensity en función
    de los ángulos theta1 y theta2 de los polarizadores de entrada y salida
    """

    # cargar el modelo
    LCD0 = LCD(modelo=modeloBurmanFase1)

    # ángulos
    theta1 = np.linspace(0, np.pi, 100)
    theta2 = np.linspace(0, np.pi, 100)

    # level de gris
    LCD0.gl_actual = 128

    # intensity para los angles determinados
    intesities = LCD0.barridoPolarizersintensity(
        theta1=theta1, theta2=theta2, modelo='DC', tecnica='matrix')

    # se dibuja a través de la clase datos matrix
    title = "level de gris: %d" % LCD0.gl_actual
    intesities = dataMatrix(x=theta1*180/np.pi, y=theta2*180/np.pi, matrix=intesities, \
                                xtext=r"$\theta_1$", ytext=r"$\theta_2$", title=title)
    intesities.draw()


def test_barridoPolarizersintensity2():
    """
    test donde se carga un level de gris y se obtiene la intensity en función
    de los ángulos theta1 y theta2 de los polarizadores de entrada y salida.
    Se realiza a través de una única función de drawing
    """

    LCD0 = LCD(modelo=modeloBurmanFase1)
    LCD0.gl_actual = 128
    intesities = LCD0.draw(
        kind='barrido_intesities', modelo='DC', tecnica='directa')


def test_generarmatrixPotencias():
    """
    Se simula un experimento donde se calculan las intesities y se guardan en un archivo
    Se utiliza para luego caracterizar mediante optimización el LCD
    """
    LCD0 = LCD(modelo=modeloBurmanFase1)

    theta1 = np.linspace(0, np.pi, 100)
    theta2 = np.linspace(0, np.pi, 100)
    LCD0.gl_actual = 0
    intesities = LCD0.barridoPolarizersintensity(
        theta1=theta1, theta2=theta2, modelo='DC', tecnica='matrix')
    intesities = dataMatrix(x=theta1*180/np.pi, y=theta2*180/np.pi, matrix=intesities, \
                    xtext="", ytext="", title='simulacion Voltaje LCD')
    intesities.add_noise(sc=0.1, I=0)
    intesities.guardarMAT(filename='simulacionmatrixExperimental')
    intesities.draw()


def test_paper():
    LCD0 = LCD(modelo=modeloLM1)

    # imprime la transmitancia
    u_escalar, ux, uy = LCD0.fields_salida(modelo='DC', tecnica='directa')
    print "u_escalar: ", u_escalar
    print "ux: ", ux
    print "uy: ", uy

    # se consigue el field en ampitud y phase
    amplitude, phase = LCD0.get_fields(
        kind='u_amplitude_phase', modelo='DC', tecnica='matrix')

    # se consigue el field completo
    u_escalar = LCD0.get_fields(
        kind='u_escalar', modelo='DC', tecnica='matrix')

    # no se consigue, pero se dibuja
    LCD0.draw(kind='u_real_imag', modelo='DC', tecnica='matrix')

    # se dibuja para comparar
    plt.figure()
    plt.subplot(1, 2, 1)
    plt.plot(LCD0.gl, amplitude, 'k')
    plt.ylim(ymin=0, ymax=max(1, max(amplitude)))
    plt.xlim(xmin=LCD0.gl[0], xmax=LCD0.gl[-1])
    plt.title('amplitude')

    plt.subplot(1, 2, 2)
    plt.plot(LCD0.gl, (phase * 180 / np.pi) % 360, 'k')
    plt.xlim(xmin=LCD0.gl[0], xmax=LCD0.gl[-1])
    plt.ylim(ymin=0)
    plt.title('phase')


def test_LCD():
    # draw_beta_delta()
    # test_drawings()

    test_modeloLM1()
    # test_modeloBurmanFase1()
    # test_modeloBurmanFase2()
    # test_modeloBurmanAmplitud()
    # test_modeloBurmanBinario()
    # test_modelo_experimental_sony()
    # test_paper()

    # test_DS()
    # test_DC()

    # test_matrices()
    # test_fields_salida()

    # test_intesidadSistema()
    test_paper()
    # test_barridoPolarizersintensity1()
    # test_barridoPolarizersintensity2()

    # test_generarmatrixPotencias()


if __name__ == '__main__':
    test_LCD()
    plt.show()
