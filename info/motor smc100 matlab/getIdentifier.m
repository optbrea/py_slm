function identifier = getIdentifier(obj_serial, nMotor)
% Acciones:
%   Devuelve la posici�n actual en unidades.
%
% Par�metros de entrada:
%   - obj_serial: referencia al objeto serial port
%
% Devuelve: 
%   - Nada

if nargin == 1 , nMotor = 1; end % n�mero de motor por defecto
flushoutput(obj_serial); %Borrado del buffer de salida.
flushinput(obj_serial);  %Borrado del buffer de entrada.
fprintf(obj_serial, [num2str(nMotor),'ID', '?']);

%lectura as�ncrona (bloqueante) con terminador
[respuestaStr, count] = fscanf(obj_serial);
if (count == 0)
    % lectura no v�lida, ha expirado el timeout sin recibir respuesta
    identifier = 'Tiempo de lectura excedido';
else
    % hemos recibido respuesta antes de que expire el timeout
    identifier = respuestaStr;
end