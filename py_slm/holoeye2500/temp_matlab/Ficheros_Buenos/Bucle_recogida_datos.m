%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FUNCI�N PARA REGISTRAR IM�GENES EN FOCO DE LA FAMILIA DE LENTES DE
% FRESNEL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 9/9/08: Sin probar

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Vector_maximos,Matriz_final,Imagenes]=Bucle_recogida_datos(f,lambdad,n_Fresnel_maximo,n_Fresnel_minimo,Path)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Inicializaci�n de la c�mara y ajuste de ganancia y tiempo de exposici�n 

%Path= 'c:/prueba_lente_fresnel_camara';

[ueyeCtrl,ueye]=startueye();  % Inicio la c�mara. 
SetHardwareGain(ueyeCtrl,0,0,0,0);       % Fijo los valores de ganancia.
SetPixelClock(ueyeCtrl,31);
SetFrameRate(ueyeCtrl,49.37);
SetExposureTime(ueyeCtrl,0.104); %0.104          % Fijo tiempo de exposici�n (debe ser �ste para que no sature en foco para 101 zonas) 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Colocaci�n a mano de la primera lente de 101 zonas

Matriz_final=Manda_imagen_modulador(f,lambdad,101);
pause(25); % Segundos para mover la pantalla virtual


% Bucle de recogida de datos
k=1;
intervalo=1;
for j=n_Fresnel_maximo:-intervalo:n_Fresnel_minimo,
Imagen=zeros(752,480); % Tama�o de la c�mara Ueye en p�xeles
Matriz_final=Manda_imagen_modulador(f,lambdad,j);
Cadena_numero_zonas=int2str(j);
Nombre_archivo_imagen=strcat(Path,'_',Cadena_numero_zonas,'.bmp');
for h=1:40,
ueye=captura(ueyeCtrl,ueye,Nombre_archivo_imagen);
%Imagen=Imagen+ueye.imagen.I;
Imagenes(:,:,h,k)=ueye.imagen.I;
end;
%Imagenes(:,:,k)=Imagen./(h);
Vector_maximos(k)=max(max(Imagenes(:,:,h,k)));
k=k+1;
pause(0.05);
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%