# !/usr/bin/env python
# -*- coding: utf-8 -*-

import os

import matplotlib.pyplot as plt

from lab_slm import *
from diffractio import degrees, mm, um
from diffractio.scalar_masks_XY import Scalar_mask_XY

s = 1.

directorio = 'C:/Users/luismiguel/bitbucket/py_slm/docs/source/TFG_rapariz/codigo/fotos_test_lente'
# createFolder(directorio)
# os.chdir(directorio)


def test_lente(radius=(2 * mm, 2 * mm),
               focal_1=(200 * mm, 200 * mm),
               z=115 * mm,
               background=False):

    SLM = lab_SLM(inicio=False,
                  range=(110 * mm, 120 * mm),
                  pos=10,
                  veloc=20 * mm / s)

    t = Scalar_mask_XY(SLM.x0, SLM.y0, SLM.wavelength)
    t.lens(r0=(0 * um, 0 * um),
           radius=radius,
           focal=focal_1,
           angle=0 * degrees)
    SLM.mask_to_rawImage(mask_XY=t, kind='phase', normalize=True)
    SLM.send_image_screen(id_screen=1, verbose=True)
    SLM.move_motor(z, kind='abs')
    Imagen = SLM.acquire_image(
        draw=True, filename='test_lente_1', remove_background=background, is_closed=False)

    del(SLM)

    return Imagen


# os.chdir('C:/Users/luismiguel/bitbucket/py_slm/docs/source/TFG_rapariz/codigo')

a = test_lente()
plt.plot(a)
