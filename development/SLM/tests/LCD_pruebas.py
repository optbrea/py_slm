# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# ------------------------------------
# Autor:    Luis Miguel Sanchez Brea
# Fecha     2012/11/01 (version 1.0)
# Licencia: copyright
# Objetivo: examples para la clase LCD
# -------------------------------------

import matplotlib.pyplot as plt
import scipy as sp

import import
import LCD
from LCD_modelos import (modelo_experimental_sony, modeloBurmanAmplitud,
                         modeloBurmanBinario, modeloBurmanFase1,
                         modeloBurmanFase2, modeloLM1)


def prueba_ajuste():
    LCD0 = LCD(modelo_experimental_sony)
    u_escalar = LCD0.get_fields(
        kind='u_escalar', modelo='DC', tecnica='matrix')
    print u_escalar
    plt.figure()
    plt.plot(np.real(u_escalar), np.imag(u_escalar), 'ko', ms=10)

    amplitude, phase = LCD0.get_fields(
        kind='u_amplitude_phase', modelo='DC', tecnica='matrix')
    plt.figure()
    plt.subplot(121)
    plt.plot(amplitude, 'ko', ms=10)
    plt.ylim(ymin=0, ymax=1)
    plt.xlim(0, 255)
    plt.subplot(122)
    plt.plot(np.unwrap(phase * 180 / np.pi) + 360, 'ko', ms=1)
    # plt.ylim(ymin=0,ymax=360)
    plt.xlim(0, 255)
    print amplitude
    print phase


def prueba_draw_intensity():
    LCD0 = LCD(modeloBurmanFase1)
    intensity = LCD0.get_fields(
        kind='intensity',
        modelo='DC',
        tecnica='matrix',
        theta1=0 * degrees,
        theta2=90 * degrees)
    print intensity
    LCD0.draw(
        kind='intensity',
        modelo='DC',
        tecnica='matrix',
        title='',
        theta1=0 * degrees,
        theta2=90 * degrees)


def prueba_optimizar_phase_doble_for(modelo=modeloBurmanFase1,
                                     num_data=15,
                                     p=4):
    LCD0 = LCD(modelo)
    Theta1 = np.linspace(0, np.pi, num_data)
    Theta2 = np.linspace(0, np.pi, num_data)
    THETA1, THETA2 = np.meshgrid(Theta1, Theta2)
    M1 = np.zeros_like(THETA1)
    for theta1, i in zip(Theta1, range(len(Theta1))):
        print i
        for theta2, j in zip(Theta2, range(len(Theta2))):
            amplitude, phase = LCD0.get_fields(
                kind='u_amplitude_phase',
                modelo='DC',
                tecnica='matrix',
                theta1=theta1,
                theta2=theta2)
            M1[i, j] = parametro_optimizar_phase(amplitude, phase, phase=p)

    M = dataMatrix(
        x=Theta1 * 180 / pi,
        y=Theta2 * 180 / pi,
        matrix=M1,
        xtext=r"$\theta_{1}$",
        ytext=r"$\theta_{2}$",
        title=r"M1")
    M.draw()
    i, j = M.i_minimum()

    print "el angle es %f, %f" % (Theta1[i] * 180 / pi, Theta2[j] * 180 / pi)
    print i, j

    LCD0.theta1 = Theta1[i]
    LCD0.theta2 = Theta2[j]
    LCD0.draw(
        kind='u_amplitude_phase',
        modelo='DC',
        tecnica='matrix',
        title='',
        theta1='',
        theta2='')


def prueba_optimizar_amplitude_doble_for(modelo=modeloBurmanFase1,
                                         num_data=15):
    LCD0 = LCD(modelo)
    Theta1 = np.linspace(0, np.pi, num_data)
    Theta2 = np.linspace(0, np.pi, num_data)
    THETA1, THETA2 = np.meshgrid(Theta1, Theta2)
    M1 = np.zeros_like(THETA1)
    for theta1, i in zip(Theta1, range(len(Theta1))):
        print i
        for theta2, j in zip(Theta2, range(len(Theta2))):
            amplitude, phase = LCD0.get_fields(
                kind='u_amplitude_phase',
                modelo='DC',
                tecnica='matrix',
                theta1=theta1,
                theta2=theta2)
            M1[i, j] = parametro_optimizar_amplitude(amplitude, phase)

    M = dataMatrix(
        x=Theta1 * 180 / pi,
        y=Theta2 * 180 / pi,
        matrix=M1,
        xtext=r"$\theta_{1}$",
        ytext=r"$\theta_{2}$",
        title=r"M1")
    M.draw()
    i, j = M.i_minimum()

    print "el angle es %f, %f" % (Theta1[i] * 180 / pi, Theta2[j] * 180 / pi)
    print i, j

    LCD0.theta1 = Theta1[i]
    LCD0.theta2 = Theta2[j]
    LCD0.draw(
        kind='u_amplitude_phase',
        modelo='DC',
        tecnica='matrix',
        title='',
        theta1='',
        theta2='')


def prueba_optimizar_amplitude_clase(modelo=modeloBurmanFase1,
                                     semilla=(107 * degrees, 79 * degrees)):
    LCD0 = LCD__optimizacion(modelo)

    values = LCD0.calculo_minimums(semilla=semilla)
    theta1, theta2 = values
    print "angles optimos: ", theta1 * 180 / pi, theta2 * 180 / pi
    print "parametro quality: ", LCD0.f_optimizar_amplitude(values)
    LCD0.draw(
        kind='u_escalar',
        modelo='DC',
        tecnica='matrix',
        title='',
        theta1=theta1,
        theta2=theta2)

    print "Optimizacion amplitude: los angles son: ", theta1, theta2

    return theta1, theta2


def prueba_optimizar_phase_clase(modelo=modeloBurmanFase1,
                                 semilla=(65 * degrees, 15 * degrees)):

    LCD0 = LCD__optimizacion(modeloLM1)

    values = LCD0.calculo_minimums(semilla=semilla)
    theta1, theta2 = values
    print "angles optimos: ", theta1 * 180 / pi, theta2 * 180 / pi
    print "parametro quality: ", LCD0.f_optimizar_phase(values, xs=20)
    LCD0.draw(
        kind='u_escalar',
        modelo='DC',
        tecnica='matrix',
        title='',
        theta1=theta1,
        theta2=theta2)
    print "Optimizacion phase: los angles son: ", theta1, theta2
    return theta1, theta2


def test_LCD():
    # prueba_ajuste()
    # prueba_draw_intensity()
    # prueba_optimizar_amplitude_doble_for(modelo=modeloBurmanFase1)
    # prueba_optimizar_phase_doble_for(modelo=modeloBurmanFase1)
    # prueba_optimizar_amplitude_clase(modelo=modelo_experimental_sony)
    prueba_optimizar_phase_clase(
        modelo=modelo_experimental_sony, semilla=(40 * degrees, 80 * degrees))


if __name__ == '__main__':
    test_LCD()
    plt.show()
