function nivel_real=corregir_nivel_fase(fase)
    desfase=linspace(0,pi,187);
    load nivel.mat
    n=cercano(desfase,fase);
    nivel_real=nivel(n);