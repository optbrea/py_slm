# -*- coding: utf-8 -*-
import time

import cv2
import screeninfo

import camera.tisgrabber as IC
from smc100 import SMC100
from holoeye import Holoeye
from utils import show_image

from config import (CONF_HOLOEYE2500, ID_SCREEN, amplification_4f, wavelength,
                    z_ini, time_waitKey)
from diffractio import degrees, mm, nm, np, plt, sp, um
from diffractio.scalar_masks_XY import Scalar_mask_XY
from diffractio.utils_optics import field_parameters
s = 1.


class SLM(object):
    """Class for sending images to

    Args:
        x (numpy.array): linear array with equidistant positions.

    Attributes:
        self.x (numpy.array): linear array with equidistant positions.
            The number of data is preferibly 2**n.
        self.y (numpy.array): linear array wit equidistant positions for y values
        self.wavelength (float): wavelength of the incident field.
        self.u (numpy.array): (x,z) complex field
        self.info (str): String with info about the simulation
    """

    def __init__(self, config_SLM, config_Camera, move_motor=False):
        """Info.

        write here
        """
        # print("init de Scalar_mask_XY")

        self.config_Camera = config_Camera
        self.config_SLM = config_SLM

        self.slm = Holoeye(config_SLM)
        self.camera = None
        self.motor = None

        self.wavelength = self.slm.wavelength

        self._init_camera()
        self.__init_motor__(vel=20 * mm / s, has_home=move_motor)

    def __close__(self):
        self.camera.StopLive()
        self.motor.close()

    def _init_camera(self, starlive=True, auto=False):
        camera = IC.TIS_CAM()

        num_x, num_y = self.config_Camera['num_pixels']
        pixel_x, pixel_y = self.config_Camera['size_pixels']

        camera.x0 = np.linspace(-num_x * pixel_x / 2, num_x * pixel_x / 2,
                                num_x)
        camera.y0 = np.linspace(-num_y * pixel_y / 2, num_y * pixel_y / 2,
                                num_y)

        camera.open(self.config_Camera['id_camera'])
        camera.SetVideoFormat(self.config_Camera['format'])
        camera.SetFrameRate(self.config_Camera['framerate'])

        if auto is False:
            camera.SetPropertySwitch("Gain", "Auto", 0)
            camera.SetPropertySwitch("Exposure", "Auto", 0)
        else:
            camera.SetPropertySwitch("Gain", "Auto", 1)
            camera.SetPropertySwitch("Exposure", "Auto", 1)

        camera.StartLive(starlive)

        self.camera = camera

    def __init_motor__(self, vel=20 * mm / s, has_home=True):
        smc100 = SMC100(1, 'COM3', silent=True)
        smc100.set_velocity(vel, check=True)
        if has_home is True:
            smc100.home()
        self.motor = smc100

    def __init_slm__(self, vel=20 * mm / s, has_home=True):
        smc100 = SMC100(1, 'COM3', silent=True)
        smc100.set_velocity(vel, check=True)
        if has_home is True:
            smc100.home()

    def camera_set_parameters(self, gain=4, exposure=-12):
        self.camera.SetPropertySwitch("Exposure", "Auto", 0)
        self.camera.SetPropertySwitch("Gain", "Auto", 0)
        self.camera.SetPropertyValue("Exposure", "Value", exposure)
        self.camera.SetPropertyValue("Gain", "Value", gain)

    def acquire_image(self,
                      draw=True,
                      remove_background=True,
                      gain_exposure=None,
                      filename='',
                      verbose=False):
        """
        gets an images from the camera1
        """

        if gain_exposure is not None:
            gain, exposure = gain_exposure

            self.camera.SetPropertySwitch("Exposure", "Auto", 0)
            self.camera.SetPropertySwitch("Gain", "Auto", 0)
            self.camera.SetPropertyValue("Exposure", "Value", exposure)
            self.camera.SetPropertyValue("Gain", "Value", gain)

        self.camera.SnapImage()
        image = self.camera.GetImage()
        image = (image[:, :, 0])
        image = image.astype('float')

        if remove_background is True:
            image_previous = self.slm.image_raw

            image_background = self.get_background()
            image_filtered = image - image_background
            image_filtered[image_filtered < 0] = 0

            self.slm.image_raw = image_previous
            self.slm.send_image_screen(verbose=verbose)

        else:
            image_filtered = image
            image_background = np.zeros_like(image)

        if draw is True:
            show_image(image_filtered, verbose)

        if filename != '':
            if draw is False:
                show_image(image_filtered)
            plt.savefig(filename)

        return image_filtered, image, image_background

    def get_background(self, level=0, draw=False, verbose=False):
        """Get an image with the SLM at 0
        This image is used as background and substracted
        TODO: Hay que ver en qué condiciones se envía la imagen
        """

        # swith modulator off

        t1 = Scalar_mask_XY(
            x=self.slm.x0, y=self.slm.y0, wavelength=self.wavelength)
        t1.one_level(level=level)

        self.slm.mask_to_rawImage(mask_XY=t1, kind='intensity', normalize=0)
        self.slm.send_image_screen(verbose=verbose)
        # Get image at a certain camera conditions
        cv2.waitKey(time_waitKey)
        self.camera.SnapImage()
        image_background = self.camera.GetImage()

        intensidad = (image_background[:, :, 0])
        intensidad = intensidad.astype('float')

        # cv2.destroyAllWindows()

        if draw is True:
            show_image()

        # return image_background
        return intensidad
