% programa para generar el mapa de fases de una fzp cuadrada trabajando en
% reflexion
function mapafase=f_generamapalente(f,lambdad,nfresnel)
%save parametrosfzpcuadrada xm fases f lambdad nivelfase errornivelfase


%% Calculo de los tama;os de las zonas cuadradas de Fresnel en funcion de
%% la focal, la longitud de onda y para un numero determinado de zonas de
%% Fresnel.

%f=0.5;
if isempty(f)
f=0.100; % en metros
end

%lambdad=650e-9;
if isempty(lambdad)
    lambdad=650e-9; % lambda del diodo laser lambdad=633e-9; % lambda del He-Ne
end

%nfresnel=101;
if isempty(nfresnel)
nfresnel=101; % suponemos que es mayor que 100
end

%nombreArchivo='lente290908.mat';
%if isempty(nombreArchivo)
%    nombreArchivo='lentecuadrada.mat';
%end


nfresnelcalculo=nfresnel;


nexterno=1; % se propaga en aire
nmedio=1.6;


indicefresnel=linspace(1,nfresnel,nfresnel);
radiofresnel=sqrt(f.*lambdad.*indicefresnel+(lambdad.*indicefresnel./2).^2);
%anchuraultimazona=radiofresnel(nfresnel)-radiofresnel(nfresnel-1);

% conversion a una placa de Fresnel con forma cuadrada
s=4; % porque tiene 4 caras
xm=radiofresnel./sqrt(1+((tan(pi/s)).^2./4));
%anchuraultimocuadrado=xm(nfresnel)-xm(nfresnel-1);

% valores de las fases de las zonas cuadradas calculadas anteriormente.
% Estan dadas en grados y han sido calculadas mediante otro programa.
fase0deg=188.67;
fasesdeg=[0 144 288 72 216 ];
fase0rad=fase0deg*pi/180;
fasesrad=fasesdeg*pi/180;


%% Calculo de las fases en los pixeles del SLM
%if isempty(nombreArchivo)
%    nombreArchivo='lentecuadrada.mat';
%end
%load(nombreArchivo);

nniveles=256; % numero de niveles de gris
fase0niv=round(fase0deg*nniveles/360);
fasesniv=round(fasesdeg*nniveles/360);

% numero de veces que se repite la secuencia de las 5 fases;
nrepeticiones=ceil((nfresnel-1)./5);
fasesmapaniv=[fase0niv];
for jj=1:nrepeticiones;
    fasesmapaniv=[fasesmapaniv fasesniv];
end



%nzonasmaximo=101;
%if isempty(nzonasmaximo)
%    nzonasmaximo=101;
%end

periodo=19e-6; % 19 micras de periodo (es el periodo de la malla)
nhor=1024; % numero de puntos en la direccion horizontal
nver=768; % numero de puntos en la direccion vertical

if max(xm)<=(nver*periodo/2)
    nzonasmaximo=nfresnel;
else
    nzonasmaximo=max(find(xm<(nver*periodo/2)));
end


indices=zeros(nver/2,1);
indicepixellimitezona=zeros(nzonasmaximo,1);
indicepixellimitezona(1)=1;
for jj=2:nzonasmaximo
    indicepixellimitezona(jj)=floor(xm(jj)./periodo);
    % proporciona el indice del pixel en el que esta el limite de la zona jj-1
    fraccionzonaanterior(jj)=xm(jj)./periodo-indicepixellimitezona(jj);
    % proporciona la fraccion de pixel que esta en la zona anterior jj-1
    indices(indicepixellimitezona(jj-1):indicepixellimitezona(jj))=jj-1;
    lineafase(indicepixellimitezona(jj-1):indicepixellimitezona(jj))=fasesmapaniv(jj-1);
    % con esta asignacion estamos colocando valores de fase a todos los
    % pixeles, habra que corregir aquellos pixeles en los que estamos
    % compartiendo zonas.
end
diagonal=lineafase;

%%
jj=2;
while jj<=nzonasmaximo
    indicepixellimites=find(indicepixellimitezona(jj:nzonasmaximo)==indicepixellimitezona(jj));
    indicepixellimites=indicepixellimites+indicepixellimitezona(jj)-1;
    nlimitesenpixel=length(indicepixellimites);
    if nlimitesenpixel==1
        lineafase(indicepixellimites(1))=fasesmapaniv(jj-1).*fraccionzonaanterior(jj)+...
            fasesmapaniv(jj).*(1-fraccionzonaanterior(jj));
        diagonal(indicepixellimites(1))=fasesmapaniv(jj-1).*fraccionzonaanterior(jj).^2+...
            fasesmapaniv(jj).*(1-fraccionzonaanterior(jj).^2);

        jj=jj+1;

    end
    if nlimitesenpixel==2
        lineafase(indicepixellimites(1))=...
            fasesmapaniv(jj-1).*fraccionzonaanterior(jj)+...
            fasesmapaniv(jj).*(fraccionzonaanterior(jj+1)-fraccionzonaanterior(jj))+...
            fasesmapaniv(jj+1).*(1-fraccionzonaanterior(jj+1));
        diagonal(indicepixellimites(1))=...
            fasesmapaniv(jj-1).*fraccionzonaanterior(jj).^2+...
            fasesmapaniv(jj).*(fraccionzonaanterior(jj+1).^2-fraccionzonaanterior(jj).^2)+...
            fasesmapaniv(jj+1).*(1-fraccionzonaanterior(jj+1).^2);
        jj=jj+2;
    end
    %pause
end

%%
lineafase=round(mod(lineafase,256));
diagonal=round(mod(diagonal,256));
mapafase1c=zeros(length(lineafase),length(lineafase));

for ii=1:length(lineafase)
    for jj=1:ii
        mapafase1c(ii,jj)=lineafase(ii);
        mapafase1c(jj,ii)=lineafase(ii);
    end
    mapafase1c(ii,ii)=diagonal(ii);
end

%%
mapafase=zeros(2*length(lineafase),2*length(lineafase));
mapafase(1:length(lineafase),1:length(lineafase))=rot90(mapafase1c,2);
mapafase(1:length(lineafase),length(lineafase)+1:2*length(lineafase))=rot90(mapafase1c,1);
mapafase(length(lineafase)+1:2*length(lineafase),1:length(lineafase))=rot90(mapafase1c,3);
mapafase(length(lineafase)+1:2*length(lineafase),length(lineafase)+1:2*length(lineafase))=mapafase1c;
% con el fin de que los niveles de gris recorran todo el rango de 256
% valores hay que crear un pixel que tenga el valor maximo
valormapafase11=mapafase(1,1);
mapafase(1,1)=255;

xx=linspace(-nzonasmaximo*periodo, nzonasmaximo*periodo, size(mapafase,1));
%figure(1);imagesc(xx,xx,mapafase);colormap('gray');axis xy;colorbar
