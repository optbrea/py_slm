Welcome to py_SLM's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   installation
   usage
   tutorial
   modules
   contributing
   authors
   history
   todo

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
