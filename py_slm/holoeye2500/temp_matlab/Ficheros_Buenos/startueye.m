%function [ueyeCtrl,ueye]=startueye(tipo,rapido)
%
%
%Luis Miguel S�nchez-Brea - 28/05/2006
%
%input: tipo-'pixel' o 'micras'
%       rapido- 1 no saca pantallas adicionales, 0 s� saca

%Reliza captura y la almacena en matriz dentro de ueye.imagen.I
%M�todo lento pues con activeX todavia no se como recoger directamente la
%imagen. Aqu� lo hago a trav�s de saveImage


function [ueyeCtrl,ueye]=startueye(tipo,rapido)

if nargin==0
    tipo='';
    rapido=1;
end


buf=0;
%% Inicializacion
ID=figure;

ueyeCtrl = actxcontrol('UEYECAM.uEyeCamCtrl.1', [0 0 1024 768]);
InitCamera(ueyeCtrl,0);
set(ID,'Selected','on')
set(gcf,'Position',[1 1 1024 700])
StartLiveVideo(ueyeCtrl,1);  %hace que la ventaja tenga video

%% informacion sobre controles
    info=actxcontrollist;
    ueye.info.metodos = methods(ueyeCtrl);

    if isempty(rapido)
        rapido = 1
    end
    
    if rapido==0
    methodsview(ueyeCtrl);
    AboutBox(ueyeCtrl);
    end
    
    if isempty(tipo)
        tipo='pixels';
    end
  
  if strcmp(tipo,'pixels')
    ueye.imagen.x='';
    ueye.imagen.y='';
    ueye.imagen.udx='pixel';
    ueye.imagen.udy='pixel';
  elseif strcmp(tipo,'micras')
    ueye.imagen.x=linspace(0,752.*6,752);
    ueye.imagen.y=linspace(0,480.*6,480);
    ueye.imagen.udx='\mum';
    ueye.imagen.udy='\mum';
  end
     
      
      
      
      ueye.imagen.ID='';
    
ueye.Ctrl.actualizarParametrosImagen=0;
ueye=getUeye(ueyeCtrl,ueye);