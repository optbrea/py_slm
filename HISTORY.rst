=======
History
=======

0.1.0 (2018-10-15)
------------------

* Start project.
* Review computer and hardware



0.1.0 (2018-10-23)
------------------

* installed computer SLM1 with python tools (windows)
* installed motor newport GUI SMC100-GUI-V2.0.0.3 - it works
* TODO: Try to use newport motor with python using 'serial' module, but not worked

0.1.0 (2018-10-23)
------------------

* installed computer IC Capture 2.4 for camera control: worked
https://github.com/TheImagingSource/tiscamera/tree/master/examples/python

https://github.com/TheImagingSource/tiscamera
