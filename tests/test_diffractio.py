from diffractio import degrees, mm, np, plt, sp, um
from diffractio.scalar_masks_XY import Scalar_mask_XY


def test_hiperellipse():

    x = np.linspace(-4 * mm, 4 * mm, 512)
    y = np.linspace(-4 * mm, 4 * mm, 512)
    wavelength = 1 * um

    t1 = Scalar_mask_XY(x, y, wavelength)

    t1.hiper_ellipse(
        r0=(0, 0),
        radius=(3 * mm, 2 * mm),
        angle=0 * degrees,
        n=[0.5, 4])
    t1.draw(kind='intensity',)


test_hiperellipse()
plt.show()
