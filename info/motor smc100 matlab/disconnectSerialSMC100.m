function disconnectSerialSMC100(obj_serial)

% Cierra la conexi�n establecida con el objeto serial pasado como
% argumento, eliminando dicho objeto del workspace y de memoria.

fclose(obj_serial);
delete(obj_serial);
clear 'obj_serial';