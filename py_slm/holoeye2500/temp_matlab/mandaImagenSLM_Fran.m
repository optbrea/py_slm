%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FUNCI�N PARA MANDAR IMAGEN AL MODULADOR LA PANTALLA VIRTUAL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 6/9/08: Sin probar
% Funciona
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [matriz_mapa_fase_correcta]=mandaImagenSLM_Fran(mapafase)

% Variables de entrada: mapafase��

% Variables de salida:
% matriz_mapa_fase_correcta, matriz de fase en p�xeles, RGB uint8.


%Se rellenan los p�xeles a los que no se les hab�a asignado valor y se pasa a formato uint8 RGB
%[matriz_mapa_fase,inicio_x,fin_x,inicio_y,fin_y]=rellenaImagenTamanoSLM(mapafase);
[matriz_mapa_fase_correcta]=Convierte_a_RGB_uint8(mapafase');

%3: Se corrige la aberraci�n de astigmatismo propia del modulador

% Ojo, la primera variable es el mapa de fases en RGB. 
% La que vale es la segunda variable

% [Mapa_fase_compensador,fase_total_recortada_rotada_gris]=Compensa_astigmatismo_SLM('','','');
% matriz_mapa_fase_corregida=mod(uint8(mapafase) - uint8(fase_total_recortada_rotada_gris),255);  % Tomar el resto equivale con mod para valores negativos en tomar 255+valor         
% matriz_mapa_fase_correcta=Convierte_a_RGB_uint8(matriz_mapa_fase_corregida');

%4: Se manda al modulador la matriz

fullscreen(matriz_mapa_fase_correcta,2);  % La pantalla virtual es la "2"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%







