# !/usr/bin/env python

# -*- coding: utf-8 -*-



import time

from pprint import pprint



import cv2

import screeninfo

import matplotlib.image as mpimg



from phyton_optics import degrees, mm, np, plt, sp, um

from phyton_optics.scalar_masks_XY import Scalar_mask_XY

from py_slm.config import CONF_HOLOEYE2500, CONF_PACKARD_BELL, CONF_IMAGING_SOURCE

from py_slm.py_slm import SLM



################

from py_slm.smc100 import SMC100

um = 1.
mm = 1000 * um

# Tests #####################################################################
def test_configure():
    # smc100 = SMC100(1, '/dev/ttyS5', silent=False)
    smc100 = SMC100(1, 'COM3', silent=True)
    smc100.reset_and_configure()
    # make sure there are no errors
    assert smc100.get_status()[0] == 0
    del smc100

def test_general():
    # smc100 = SMC100(1, '/dev/ttyS5', silent=False)
    smc100 = SMC100(1, 'COM3', silent=True)
    print(smc100.get_position())
    smc100.home()
    # make sure there are no errors
    assert smc100.get_status()[0] == 0
    smc100.move_relative(1 * mm, verbose=True)
    smc100.move_relative(1 * mm, verbose=True)
    assert smc100.get_status()[0] == 0
    pos = smc100.get_position(verbose=True)
    assert abs(pos / 1000. - 2) < 0.001
    assert smc100.get_status()[0] == 0
    del smc100


ms = 1.

seconds = 1000 * ms

s = 1.
"""

Conocer los monitores disponibles. El SLM se lee como un segundo monitor al

que le enviaremos la imagen, por lo que con el siguiente código deberíamos

obtener la información de ambos monitores.

"""

screeninfo.get_monitors()



"Llamamos a la clase SLM"

slm = SLM(config_SLM=CONF_HOLOEYE2500, config_Camera=CONF_IMAGING_SOURCE)

"""

Obtenemos los parámetros para la configuración de la cámara. Estos están ya

en la clase de SLM

"""

x0 = slm.x0

y0 = slm.y0

wavelength = slm.wavelength

print(x0.shape, y0.shape)

slm.camera1.StartLive()

"""

Obtenemos la imagen-ruido de fondo. Esto se usará para eliminar este background

cuando se obtenga la imagen de la máscara en la cámara.

"""

slm.get_background()

# slm.show_background()

"""

Introducimos la máscara que queremos reproducir en el SLM. Para ello usaremos

las clases de Scalar_mask_XY y Ejercicios_TFG.



Empezaremos con un círculo simple.

"""

radius = (.5 * mm, .5 * mm)



t1 = Scalar_mask_XY(x=x0, y=y0, wavelength=wavelength)

t1.grating_2D_ajedrez(period=0.038*10 * mm,

                      amin=0,

                      amax=255,

                      phase=0 * np.pi,

                      x0=0,

                      fill_factor=0.5,

                      angle=0 * degrees)

t1.circle(r0=(0 * um, 0 * um), radius=radius, angle=0 * degrees)

# t1.lens()
# t1.lens(r0=(0,0), radius=radius,
#         focal=(5*mm,5*mm), angle=0 * degrees, mask=True)
# t1.u = t1.u * 255

# t1.draw(kind='field', has_colorbar='')



# "Creamos la imagen a partir de la máscara creada."

# slm.mask_to_rawImage(mask_XY=t1, kind='intensity', normalize=True)

#
# "Enviamos la imagen al SLM."

# slm.send_image_screen(id_screen=1, verbose=False)



"""

El problema reside en que al poner el time.sleep(), la funcion

send_image_screen entra en pausa y no se ejecuta hasta que acabe el .sleep()

"""


# smc100 = SMC100(1, 'COM3', silent=True)
# smc100.reset_and_configure()
# make sure there are no errors
# assert smc100.get_status()[0] == 0
# del smc100

## Motores
test_configure()
smc100 = SMC100(1, 'COM3', silent=True)
print(smc100.get_position())
print(smc100.get_velocity(verbose=True))
smc100.home()
smc100.move_absolute(116.35*mm)
# smc100.move_absolute(125*mm)
# smc100.set_velocity(5*mm/s, check=True)
# smc100.move_relative(-.25 * mm, verbose=True)

raw_input("Estoy en HOME");

for j in range(0,50):

    fname = 'AmpFase_'+str(j)+'_'+str(smc100.get_position())+'.png';
    # fname = 'circ05_'+str(j)+'_'+str(smc100.get_position())+'.png';
    # raw_input("Siguiente captura");
    # cv2.waitKey(300)
    imagen1 = slm.acquire_image(draw=True,
                                remove_background=False,
                                filename=fname,
                                is_closed=True)
    # imagen1 = slm.acquire_image(draw=True,
    #                             remove_background=False,
    #                             filename='imagen_camara.png',
    #                             is_closed=True)
    cv2.waitKey(500)
    # assert smc100.get_status()[0] == 0
    # smc100.move_relative(-1.5 * mm, verbose=True)
    # img_c = mpimg.imread(fname)
    # cv2.waitKey(500)
    # plt.imshow(img_c)
    # cv2.waitKey(500)
    # plt.show()

smc100.set_velocity(20*mm/s, check=True)
smc100.home()
smc100.close()
