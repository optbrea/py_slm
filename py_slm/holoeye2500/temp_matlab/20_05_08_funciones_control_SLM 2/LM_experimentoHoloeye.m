% funci�n que carga los datos en el holoeye
%dentro del bucle for hay que tomar las medidas experimentales.

N=50;      %numero de zonas
f=1;   %focal de la lente en m
lambda=.68e-6;  %longitud de onda
nombreArchivo='LenteCuadradaHoloeye.mat';   %nombre de archivo temporal donde se guardan datos

GenerarLenteCuadrada(f,lambda,N,nombreArchivo)

posreal=[1 1 1024 768];
posholoeye=[-1025 0 1024 768];

ID=figure;  %preparardibujo
axis equal
axis square
%for i=4:N
i=N;
    mapafase=generarZonasLente(i,nombreArchivo);
    [matriz_mapa_fase,inicio_x,fin_x,inicio_y,fin_y,ID]=CargarLenteCuadradaSLM(mapafase,posholoeye,'');
   % pause

    %aqui hay que grabar lo que tengamos en la c�mara y guardarlo o procesarlo
    %preguntar Fran sobre programa de captura con ueye.

    %preguntar LuisMiguel, cuando se enfoque sobre qu� hay que capturar.
%end

