from py_slm.smc100 import SMC100

um = 1.
mm = 1000 * um

# Tests #####################################################################


def test_configure():
    # smc100 = SMC100(1, '/dev/ttyS5', silent=False)
    smc100 = SMC100(1, 'COM3', silent=True)
    smc100.reset_and_configure()
    # make sure there are no errors
    assert smc100.get_status()[0] == 0
    del smc100


def test_general():
    # smc100 = SMC100(1, '/dev/ttyS5', silent=False)
    smc100 = SMC100(1, 'COM3', silent=True)
    print(smc100.get_position())

    smc100.home()

    # make sure there are no errors
    assert smc100.get_status()[0] == 0

    smc100.move_relative(1 * mm, verbose=True)
    smc100.move_relative(1 * mm, verbose=True)

    assert smc100.get_status()[0] == 0

    pos = smc100.get_position(verbose=True)

    assert abs(pos / 1000. - 2) < 0.001

    assert smc100.get_status()[0] == 0

    del smc100


if __name__ == '__main__':
    test_configure()
    test_general()
