# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# ------------------------------------
# Autor:    Luis Miguel Sanchez Brea
# Fecha     2012 (version 1.0)
# Licencia: copyright
# Objetivo: funciones para polarización, en especial para el
#           LCD (J. Campos y F. Perez Quintian
# -------------------------------------
"""
    Se crea la simulación de un LCD, según la tesis de Máster de Airel Burman
    Caracterización y control de un microdisplay como modulador espacial de luz
    Universidad de buenos Aires (2010).

    Aquí se simula el comportamiento de un úico pixel.
    Esto nos servirá posteriormente para que, una vez caracterizado el LCD,
    podamos utilizarlo  para simular LCD, con una matrix de píxeles.

    Los parámetros son los parámetros que caracterizan el modulador
    gl: levels de gris
    alfa: angle de giro (twist) ángulo total de rotation de las moléculas
    beta(V): phase producido definido como pi*(ne-no)*d/lambda
    psi:  eje director de la cara de entrada
    delta: phase
    wavelength: longitud de onda a la que se trabaja
"""

from scipy.optimize import fmin, fmin_cg

from data_generator.data1D import data1D
from data_generator.dataMatrix import dataMatrix
from diffractio import degrees, np, plt, sp, um
from diffractio.utils.general import ndgrid
from LCD_modelos import *


class LCD(object):
    def __init__(self, modelo):
        """
            Se crea la simulación de un LCD, según la tesis de Máster de Airel Burman
            'Caracterización y control de un microdisplay como modulador espacial de luz'
            Universidad de buenos Aires (2010).

            Los parámetros son los parámetros que caracterizan el modulador
            gl: levels de gris
            alfa: angle de giro (twist) ángulo total de rotation de las moléculas a través del material
            beta(V): phase producido definido como pi*(ne-no)*d/lambda
            psi:  eje director de la cara de entrada
            delta: phase
            wavelength: longitud de onda a la que se trabaja
        """

        self.alfa = modelo['alfa']
        self.gl = modelo['gl']
        self.beta = modelo['beta']
        self.psi = modelo['psi']
        self.delta = modelo['delta']
        self.wavelength = modelo['wavelength']  #la longitud de onda

        self.theta1 = modelo['theta1']
        self.theta2 = modelo['theta2']

        self.haz_incidente = modelo['haz_incidente']

        self.gl_actual = 0  #level de gris actual

    def LC(self):
        """matrix LC según pág 39 de la tesis"""

        alfa = self.alfa
        beta = self.beta[self.gl_actual]
        Gamma = sp.sqrt(alfa**2 + beta**2)

        return sp.matrix(sp.array([[ cos(Gamma) - 1.j * beta * sin(Gamma) / Gamma, alfa * sin(Gamma) / Gamma],\
            [-alfa * sin(Gamma) / Gamma, cos(Gamma) + 1.j * beta * sin(Gamma) / Gamma]]), dtype=complex)

    def DS(self, tecnica='directa'):
        """matrix del microDisplay D_s (LCD) al aplicar voltaje modelo de saleh-LU
        según ec. 2.42  pág 39
        según ec. 2.45  pág 42

        #tambien se calcula de forma explícita en A.1.1 a través de z5 y z6 (más rápido) A.24 de la página 110
        gl: level de gris que utilizamos
        tecnica: 'matrix' o 'directa. En la tesis de Burman hace las 2 y habría que compararlas

        """

        alfa = self.alfa
        beta = self.beta[self.gl_actual]
        psi = self.psi
        Gamma = sp.sqrt(alfa**2 + beta**2)

        if tecnica == 'directa':
            return rotation(theta=-psi) * rotation(
                theta=-alfa) * self.LC() * rotation(theta=psi)

        elif tecnica == 'matrix':
            z5 = cos(Gamma) * cos(alfa) + alfa * sin(Gamma) * sin(
                alfa) / Gamma - 1.j * beta * sin(Gamma) * cos(alfa +
                                                              2 * psi) / Gamma
            z6 = cos(Gamma) * sin(alfa) - alfa * sin(Gamma) * cos(
                alfa) / Gamma - 1.j * beta * sin(Gamma) * sin(alfa +
                                                              2 * psi) / Gamma

            ds = sp.zeros((2, 2), dtype=complex)
            ds[0, 0] = z5
            ds[0, 1] = -sp.conj(z6)
            ds[1, 0] = z6
            ds[1, 1] = sp.conj(z5)
            return ds

    def DC(self, tecnica='directa'):
        """
        Simulacion del LCD, a través de la matrix. Está definido en las ecuaciones A.33 (pag 112)
        self.gl_actual: level de gris que utilizamos
        tecnica: 'matrix' o 'directa. En la tesis de Burman hace las 2 y habría que compararlas
        """

        alfa = self.alfa
        beta = self.beta[self.gl_actual]
        delta = self.delta[self.gl_actual]
        psi = self.psi
        Gamma = sp.sqrt(alfa**2 + beta**2)

        if tecnica == 'directa':
            S = polarizer_retarder(phase=delta, theta=0 * degrees)
            dc = rotation(theta=-psi) * rotation(
                theta=-alfa) * S * self.LC() * S * rotation(theta=psi)

        elif tecnica == 'matrix':
            z9R = (cos(Gamma) * cos(delta) - beta * sin(Gamma) * sin(delta) /
                   Gamma) * cos(alfa) + alfa * sin(Gamma) * sin(alfa) / Gamma
            z9i = (-beta * sin(Gamma) * cos(delta) / Gamma -
                   cos(Gamma) * sin(delta)) * cos(alfa + 2 * psi)

            z10R = (cos(Gamma) * cos(delta) - beta * sin(Gamma) * sin(delta) /
                    Gamma) * sin(alfa) - alfa * sin(Gamma) * cos(alfa) / Gamma
            z10i = (-beta * sin(Gamma) * cos(delta) / Gamma -
                    cos(Gamma) * sin(delta)) * sin(alfa + 2 * psi)

            z9 = z9R + 1.j * z9i
            z10 = z10R + 1.j * z10i

            dc = sp.zeros((2, 2), dtype=complex)
            dc[0, 0] = z9
            dc[0, 1] = -sp.conj(z10)
            dc[1, 0] = z10
            dc[1, 1] = sp.conj(z9)

        return dc

    def field_salida(self,
                     modelo='DC',
                     tecnica='directa',
                     theta1='',
                     theta2=''):
        """
        Ecuación A.37
        En esta se calcula el en función de los parámetros del haz
        devuelve ux,uy
        solo para self.gl_actual.
        lleva incorporado el haz incidente
        """

        if theta1 == '':
            theta1 = self.theta1
        if theta2 == '':
            theta2 = self.theta2

        if modelo == 'DS':
            u0 = polarizer_linear(theta=theta2) * self.DS(
                tecnica=tecnica) * polarizer_linear(
                    theta=theta1) * self.haz_incidente
        elif modelo == 'DC':
            u0 = polarizer_linear(theta=theta2) * self.DC(
                tecnica=tecnica) * polarizer_linear(
                    theta=theta1) * self.haz_incidente

        return sp.ravel(u0)  #esto devuelve ux,uy

    def intensity_salida(self,
                         modelo='DC',
                         tecnica='directa',
                         theta1='',
                         theta2=''):
        """
        Ecuación A.37
        En esta se calcula el en función de los parámetros del haz
        devuelve ux,uy
        solo para self.gl_actual
        lleva incorporado el haz incidente
        """

        if theta1 == '':
            theta1 = self.theta1
        if theta2 == '':
            theta2 = self.theta2

        u0 = self.field_salida(
            modelo=modelo, tecnica=tecnica, theta1=theta1, theta2=theta2)
        ux = u0[0]
        uy = u0[1]

        return abs(ux)**2 + abs(uy)**2

    def fields_salida(self,
                      modelo='DC',
                      tecnica='directa',
                      theta1='',
                      theta2=''):
        """
        devuelve la transmitancia para un modelo
        para todos los levels de gris.
        """

        if theta1 == '':
            theta1 = self.theta1
        if theta2 == '':
            theta2 = self.theta2

        ux = sp.zeros(self.gl.shape, dtype=complex)
        uy = sp.zeros(self.gl.shape, dtype=complex)
        u_escalar = sp.zeros(self.gl.shape, dtype=complex)

        gl_inicial = self.gl_actual

        for i, gl_actual in zip(range(len(self.gl)), self.gl):
            self.gl_actual = gl_actual
            u0 = self.field_salida(
                modelo=modelo, tecnica=tecnica, theta1=theta1, theta2=theta2)
            ux[i] = u0[0]
            uy[i] = u0[1]

        u_real = sp.real(ux) + sp.real(uy)
        u_imag = sp.imag(ux) + sp.imag(uy)
        u_escalar = u_real + 1j * u_imag
        """
        amplitude=sp.sqrt(abs(ux)**2+abs(uy)**2)
        phasex=sp.arctan2(sp.imag(ux), sp.real(ux))
        phasey=sp.arctan2(sp.imag(uy), sp.real(uy))
        phase=phasey-phasex
        u_escalar=amplitude*sp.exp(1.j*phase)
        """

        self.gl_actual = gl_inicial

        return u_escalar, ux, uy

    def get_matrices(self,
                     modelo='DC',
                     tecnica='directa',
                     theta1='',
                     theta2=''):
        """
        saca las matrices 2x2 del modulo polarizador2*LCD*polarizador1
        sin la iluminacion
        para todos los levels de gris
        """
        if theta1 == '':
            theta1 = self.theta1
        if theta2 == '':
            theta2 = self.theta2

        matrices = sp.zeros((2, 2, len(self.gl)), dtype=complex)
        gl_inicial = self.gl_actual

        for i, gl_actual in zip(range(len(self.gl)), self.gl):
            self.gl_actual = gl_actual
            if modelo == 'DS':
                t_vectorial = polarizer_linear(theta=theta2) * self.DS(
                    tecnica=tecnica) * polarizer_linear(theta=theta1)
            elif modelo == 'DC':
                t_vectorial = polarizer_linear(theta=theta2) * self.DC(
                    tecnica=tecnica) * polarizer_linear(theta=theta1)
            matrices[:, :, i] = t_vectorial

        return matrices  #devuelve 2 x 2 x levels_gris

    def get_fields(self,
                   kind='u_escalar',
                   modelo='DC',
                   tecnica='directa',
                   theta1='',
                   theta2=''):
        """
        calcula y obtiene el field para todo el array gl
        kind= ' u_escalar', 'u_vectorial', 'u_real_imag', 'u_amplitude_phase', 'intensity'
        """
        if theta1 == '':
            theta1 = self.theta1
        if theta2 == '':
            theta2 = self.theta2

        u_escalar, ux, uy = self.fields_salida(
            modelo=modelo, tecnica=tecnica, theta1=theta1, theta2=theta2)

        if kind == 'u_escalar':
            return u_escalar

        if kind == 'u_vectorial':
            return ux, uy

        if kind == 'u_real_imag':
            u_real = sp.real(u_escalar)
            u_imag = sp.imag(u_escalar)
            return u_real, u_imag

        if kind == 'u_amplitude_phase':
            amplitude = abs(u_escalar)
            phase = sp.arctan2(sp.imag(u_escalar), sp.real(u_escalar))
            return amplitude, phase

        if kind == 'intensity':
            intensity = abs(ux)**2 + abs(uy)**2
            #intensity = abs(u_escalar)**2
            return intensity


#           """
#           ¡cuidado! no coincide con la Ecuación 2.60
#           En esta se calcula la intensity en función de los parámetros del haz
#           """
#
#           u0 = self.field(modelo=modelo, tecnica=tecnica)
#           intensity = abs(u0[0]) ** 2 + abs(u0[1]) ** 2
#
#           if False:
#               """
#               Ecuación 2.60
#               En esta se calcula la intensity en función de los parámetros del haz
#               """
#
#               alfa = self.alfa
#               beta = self.beta[self.gl_actual]
#               delta = self.delta[self.gl_actual]
#               psi = self.psi
#
#               Gamma = sp.sqrt(alfa ** 2 + beta ** 2)
#               i1 = (cos(Gamma) * cos(delta) - beta * sin(Gamma) * sin(delta) / Gamma) * cos(alfa - theta2 + theta1)
#               i2 = alfa * sin(Gamma) * sin(alfa - theta2 + theta1) / Gamma
#               i3 = (-beta * sin(Gamma) * cos(delta) / Gamma - cos(Gamma) * sin(delta)) * cos(alfa + 2 * psi - theta1 - theta2)
#
#               intensity = (i1 + i2) ** 2 + i3 ** 2
#               intensity = intensity / 2



    def barridoPolarizersintensity(self, theta1=sp.linspace(0, sp.pi, 100), theta2=sp.linspace(0, sp.pi, 100),  \
            modelo='DC', tecnica='directa'):
        """
        Se tiene un sistema con unas conditiones determinadas entre dos polarizadores
        y se hace un barrido de estos polarizadores.
        El resultado es una matrix de intesities para los 255 levels de gris, por lo que la matrix es 3D
        """

        theta1_inicial = self.theta1
        theta2_inicial = self.theta2

        Theta1, Theta2 = ndgrid(theta1, theta2)
        intensityes = sp.zeros_like(Theta1)
        for i, t1 in zip(range(len(theta1)), theta1):
            for j, t2 in zip(range(len(theta1)), theta1):
                self.theta1 = t1
                self.theta2 = t2
                intensityes[i, j] = self.intensity_salida(
                    modelo=modelo, tecnica=tecnica)

        self.theta1 = theta1_inicial
        self.theta2 = theta2_inicial

        return intensityes

    def draw(self,
             kind='barrido_intesities',
             modelo='DC',
             tecnica='matrix',
             title='',
             theta1='',
             theta2=''):
        """
        kind= 'beta',  'delta', 'beta_delta', 'barrido_intesities', ' u_escalar', 'u_vectorial', \
                'u_real_imag', 'u_amplitude_phase', 'intensity'
        """
        if kind == 'u_amplitude_phase':
            kind = 'u_escalar'

        if theta1 == '':
            theta1 = self.theta1
        if theta2 == '':
            theta2 = self.theta2

        if kind == 'beta':
            b = data1D(
                x=self.gl,
                y=self.beta * 180 / sp.pi,
                xtext=r'$g.l.$',
                ytext=r"$\beta(V) ^{0}$",
                title="")
            b.draw()
            plt.ylim(ymin=0)

        if kind == 'delta':
            d = data1D(
                x=self.gl,
                y=self.delta * 180 / sp.pi,
                xtext=r'$g.l.$',
                ytext=r'$\delta(V) ^{0}$',
                title="")
            d.draw()
            plt.ylim(ymin=0)

        if kind == 'beta_delta':
            b = data1D(
                x=self.gl,
                y=self.beta * 180 / sp.pi,
                xtext=r'$g.l.$',
                ytext=r"$\beta(V) ^{0}$",
                title="")
            plt.ylim(ymin=0)
            d = data1D(
                x=self.gl,
                y=self.delta * 180 / sp.pi,
                xtext=r'$g.l.$',
                ytext=r'$\delta(V) ^{0}$',
                title="")
            plt.ylim(ymin=0)

            draw_varios(data1D = (b,d), kinds = ('k', 'k--'), labels = (r"$\beta(V) ^{0}$", r'$\delta(V) ^{0}$'), \
                        legend = True)
            plt.ylabel('angle', fontsize=20)

        if kind == 'barrido_intesities':
            theta1 = sp.linspace(0, sp.pi, 100)
            theta2 = sp.linspace(0, sp.pi, 100)

            title = "g.l. = %s" % (self.gl_actual)
            intesities = self.barridoPolarizersintensity(
                modelo=modelo, tecnica=tecnica)
            intesities = dataMatrix(x=theta1*180/sp.pi, y=theta2*180/sp.pi, matrix=intesities, \
                                xtext=r"$\theta_1 (degrees)$", ytext=r"$\theta_2 (degrees)$", title=title)
            intesities.draw(orientation='vertical')

        if kind == 'u_escalar':
            amplitude, phase = self.get_fields(
                kind='u_amplitude_phase',
                modelo=modelo,
                tecnica=tecnica,
                theta1=theta1,
                theta2=theta2)

            plt.figure(figsize=(12, 6), facecolor='w', edgecolor='k')
            plt.subplot(1, 2, 1)
            plt.plot(self.gl, amplitude, 'k', lw=2)
            plt.xlabel(r'$g.l.$', fontsize=20)
            plt.ylabel('amplitude', fontsize=20)
            plt.ylim(ymin=0, ymax=max(1, max(amplitude)))
            plt.xlim(xmin=self.gl[0], xmax=self.gl[-1])

            plt.subplot(1, 2, 2)
            plt.plot(self.gl, (phase * 180 / sp.pi) % 360, 'k', lw=2)
            plt.xlabel(r'$g.l.$', fontsize=20)
            plt.ylabel('phase', fontsize=20)
            plt.xlim(xmin=self.gl[0], xmax=self.gl[-1])
            plt.ylim(ymin=0, ymax=360)

        if kind == 'u_real_imag':
            u_real, u_imag = self.get_fields(
                kind='u_real_imag',
                modelo=modelo,
                tecnica=tecnica,
                theta1=theta1,
                theta2=theta2)

            b = data1D(
                x=u_real,
                y=u_imag,
                xtext='$real(u)$',
                ytext="$imag(u)$",
                title="")
            b.draw()
            plt.axes().set_aspect('equal')
            todo = sp.concatenate((abs(u_real), abs(u_imag)))
            maximum = max(todo.ravel())
            rango = max(maximum, 1)
            plt.ylim(ymin=-rango, ymax=rango)
            plt.xlim(xmin=-rango, xmax=rango)
            plt.grid(True)

            plt.suptitle(title, fontsize=24)

        if kind == 'intensity':
            intensity = self.get_fields(
                kind='intensity',
                modelo=modelo,
                tecnica=tecnica,
                theta1=theta1,
                theta2=theta2)

            b = data1D(
                x=self.gl,
                y=intensity,
                xtext=r'$g.l.$',
                ytext=r"$intensity$",
                title="")
            b.draw()
            plt.ylim(ymin=0)


class LCD__optimizacion(LCD):
    def calculo_minimums(self,
                         semilla=(0 * degrees, 0 * degrees),
                         kind_optimizacion='amplitude'):
        """ (list) -> (list)
        Descripción: Cálculo de los parámetros internos mínimos del LCD, alfa, beta máxima, psi a través de la
        minimización por el algoritmo de descenso de colinas del error (fórmula 2.61 pag 60)
        Entradas:
        xs = variable de empaquetamiento
        Salidas:
        minimums = lista con tres elementos; alfa, beta_maxima y psi
        """
        # ([-79.5 * radianes, 114.3 * radianes, -1.8 * radianes]) Valores que le sale a Burman

        if kind_optimizacion == 'amplitude':
            minimums = fmin_cg(self.f_optimizar_amplitude, semilla)
        else:
            minimums = fmin_cg(self.f_optimizar_phase, semilla)

        return minimums

    def f_optimizar_amplitude(self, xs):
        """ (list) -> (float)
        Descripción: función para empaquetar las variables y compute el error (fórmula 2.61 pag 60)
        Entradas:
        xs = variable de empaquetamiento
        Salidas:
        error = Diferencia entre la intensity teórica y experimental
        """
        # Parámetros desconocidos del LCD
        theta1, theta2 = xs

        amplitude, phase = self.get_fields(
            kind='u_amplitude_phase',
            modelo='DC',
            tecnica='matrix',
            theta1=theta1,
            theta2=theta2)
        m1 = parametro_optimizar_amplitude(amplitude, phase)

        return m1

    def f_optimizar_phase(self, xs, n=4):
        """ (list) -> (float)
        Descripción: función para empaquetar las variables y compute el error (fórmula 2.61 pag 60)
        Entradas:
        xs = variable de empaquetamiento
        Salidas:
        error = Diferencia entre la intensity teórica y experimental
        """
        # Parámetros desconocidos del LCD
        theta1, theta2 = xs

        amplitude, phase = self.get_fields(
            kind='u_amplitude_phase',
            modelo='DC',
            tecnica='matrix',
            theta1=theta1,
            theta2=theta2)
        m1 = parametro_optimizar_phase(amplitude, phase, n)

        return m1


def parametro_optimizar_phase(amplitude, phase, n=4):
    m1 = (phase.max() - phase.min()) / sp.std(amplitude)
    m1 = m1 * (amplitude.mean())**n
    return -m1


def parametro_optimizar_amplitude(amplitude, phase):
    m1 = (amplitude.max() / amplitude.min()) / sp.std(phase)
    return -m1


class LCD_compute_parametros_experimentales(object):
    """se cargan matrices de datos para cada voltaje
    se calculan las matrices
        'alfa': 90 * degrees,
        'beta': beta_example(gl=sp.array(range(256)), betaMax=360 * degrees, betaMin=0 * degrees),
        'psi': 0*degrees,
        'delta': delta_example(gl=sp.array(range(256)), deltaMax=45 * degrees, deltaMin=0 * degrees),
    que se utilizarán para caracterizar el comportamiento real de un modulador
    """

    def __init__(self):
        """se necesita un modelo de LCD
            se necesita una curva ideal
            el resultado debe ser unos parámetros theta1 y theta2 que acerquen lo máximo posible
            el resultado teórico con la curva ideal
        """
        pass

    def cargar_datos(self, LCD_teorico, graficaExperimental):
        """
            carga Modelo teórico y experimental
        """

        self.LCD_teorico = LCD_teorico
        self.graficaExperimental = graficaExperimental

        I_experimental = dataMatrix(
            xtext=r" ", ytext=r" ", title='simulacion Voltaje LCD')
        I_experimental.cargar_mat(graficaExperimental)
        self.I_experimental = I_experimental

    def error(self):
        theta1 = self.I_experimental.x * sp.pi / 180
        theta2 = self.I_experimental.y * sp.pi / 180
        I_teorica = self.LCD_teorico.barridoPolarizersintensity(
            theta1=theta1, theta2=theta2, modelo='DC', tecnica='matrix')

        error = sum(sum((self.I_experimental.Z - I_teorica)**2)) / (
            self.I_experimental.Z.shape[0] * self.I_experimental.Z.shape[1])
        return error


def error(p, LCD_teorico, I_experimental):

    alfa = p[0]
    beta_max = p[1]
    psi = p[2]

    LCD_teorico.alfa = alfa
    LCD_teorico.beta = beta
    LCD_teorico.psi = psi

    theta1 = I_experimental.x * sp.pi / 180
    theta2 = I_experimental.y * sp.pi / 180
    I_teorica = LCD_teorico.barridoPolarizersintensity(
        theta1=theta1, theta2=theta2, modelo='DC', tecnica='matrix')
    error = sum(sum(
        (I_experimental.Z - I_teorica)**
        2)) / (I_experimental.Z.shape[0] * I_experimental.Z.shape[1])
    return error


"""
    A, B, x0, period = p
    return (y - (A * sp.sin(2 * sp.pi * (x - x0) / period) + B)) ** 2

"ajuste propiamente dicho"
plsq, cov_x = leastsq(error, [1.0, 1.0, 1.0, 1.0], args=(datosExperimentales.y, datosExperimentales.x))
#plsq (array([ 5.11878922, 1.14629313]), 'Both actual and predicted relative reductions in the sum of squares\n are at most 0.000000')
print plsq



def error(p, y, x):
    A, B, x0, period = p
    return (y - (A * sp.sin(2 * sp.pi * (x - x0) / period) + B)) ** 2

"ajuste propiamente dicho"
plsq, cov_x = leastsq(error, [1.0, 1.0, 1.0, 1.0], args=(datosExperimentales.y, datosExperimentales.x))
#plsq (array([ 5.11878922, 1.14629313]), 'Both actual and predicted relative reductions in the sum of squares\n are at most 0.000000')
print plsq
"""

if __name__ == '__main__':
    import sys
    sys.path.append('tests/')
    from LCD_tests import test_LCD
    test_LCD()
    plt.show()
