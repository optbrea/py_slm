function ueye=setUeye(ueyeCtrl,ueye)
buf=0;
    ueye.param.HardwareGain=1;

%% Actuacion sobre la camara
SetBrightness(ueyeCtrl, ueye.param.Brightness);
SetContrast(ueyeCtrl, ueye.param.Contrast);
SetFrameRate(ueyeCtrl, ueye.param.FrameRate);
SetGamma(ueyeCtrl, ueye.param.Gamma);
SetImagePosition(ueyeCtrl, ueye.param.ImagePosition(1), ueye.param.ImagePosition(2));  %mirar archivo que hay restricciones: x multiplo de 16 y multiplo de 2
SetImageSize(ueyeCtrl, ueye.param.ImageSize(1), ueye.param.ImageSize(2));
%SetWhiteBalance(ueyeCtrl, 0);
SetHardwareGain(ueyeCtrl,ueye.param.HardwareGain,buf,buf,buf);   %de 0 a 100
SetPixelClock(ueyeCtrl,  ueye.param.PixelClock);  % 10 a 30 MHz
SetExposureTime(ueyeCtrl, ueye.param.ExposureTime); %ms
SetEdgeEnhancement(ueyeCtrl,ueye.param.EdgeEnhancement);
%modoTrigger=8;
%SetExternalTrigger(ueyeCtrl,modo)  % 0: off %1 hardware   8: Software

 
