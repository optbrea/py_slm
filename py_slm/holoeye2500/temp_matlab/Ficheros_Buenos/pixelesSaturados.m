function [N,posx,posy]=pixelesSaturados(I1)

[posx,posy]=find(I1>=250);
N=length(posx);