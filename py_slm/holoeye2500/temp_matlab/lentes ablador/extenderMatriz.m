function [matrizExtendida,posicionMatriz]=extenderMatriz(matriz, extension)

[size_inicial_x,size_inicial_y]=size(matriz);

    size_x=extension.x;
    size_y=extension.y;


centro_x_ventana=(size_x/2)-1; % Centros en x,y.
centro_y_ventana=(size_y/2)-1;

inicio_x=centro_x_ventana-(size_inicial_x/2); % Defino los tama�os de la ventana y supongo que la lente tiene un n�mero de p�xeles par
fin_x=centro_x_ventana+(size_inicial_x/2)-1; % Hay que incluir al cero de la ventana
inicio_y=centro_y_ventana-(size_inicial_y/2);
fin_y=centro_y_ventana+(size_inicial_y/2)-1;

matrizExtendida=zeros(size_x,size_y);                  %la matriz inicial es todo ceros
matrizExtendida(inicio_x:fin_x,inicio_y:fin_y)=double(matriz);
posicionMatriz.x=[inicio_x:fin_x];
posicionMatriz.y=[inicio_y:fin_y];