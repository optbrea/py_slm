# !/usr/bin/env python3
# -*- coding: utf-8 -*-
# ------------------------------------
# Autor:    Luis Miguel Sanchez Brea
# Fecha     2012/11/01 (version 1.0)
# Licencia: copyright
# Objetivo: examples para la clase LCD
# -------------------------------------

import LCD
from LCD import *


def optimizar_amplitude_clase(modelo, semilla):
    LCD0 = LCD__optimizacion(modelo)

    values = LCD0.calculo_minimums(semilla=semilla)
    theta1, theta2 = values
    print "angles optimos: ", theta1 * 180 / pi, theta2 * 180 / pi
    print "parametro quality: ", LCD0.f_optimizar_amplitude(values)
    LCD0.draw(
        kind='u_escalar',
        modelo='DC',
        tecnica='matrix',
        title='',
        theta1=theta1,
        theta2=theta2)

    print "Optimizacion amplitude: los angles son: ", theta1 / degrees, theta2 / degrees
    return theta1, theta2


def optimizar_phase_clase(modelo, semilla, n):
    #semilla =  theta1, theta2
    LCD0 = LCD__optimizacion(modelo)
    values = LCD0.calculo_minimums(semilla=semilla)
    theta1, theta2 = values
    print "angles optimos: ", theta1 * 180 / pi, theta2 * 180 / pi
    print "parametro quality: ", LCD0.f_optimizar_phase(values, n=n)
    LCD0.draw(
        kind='u_escalar',
        modelo='DC',
        tecnica='matrix',
        title='',
        theta1=theta1,
        theta2=theta2)
    print "Optimizacion phase: los angles son: ", theta1 / degrees, theta2 / degrees
    return theta1, theta2


if __name__ == '__main__':
    #optimizar_amplitude_clase(modelo=modeloBurmanFase2, semilla=(72*degrees,24*degrees))
    optimizar_phase_clase(
        modelo=modeloBurmanFase1, semilla=(35 * degrees, 60 * degrees),
        n=8)  #semilla=(35*degrees,60*degrees)
    """
    LCD0 = LCD__optimizacion(modeloBurmanFase1)
    amplitude,phase=LCD0.get_fields(kind='u_amplitude_phase')
    #LCD0.draw(kind='u_escalar')
    M1=parametro_optimizar_amplitude,phase)
    print "M1=",M1
    optimizar_phase_clase(modelo=modeloBurmanFase1, semilla=(72*degrees,24*degrees),n=0) #semilla=(35*degrees,60*degrees)
    """
    plt.show()
