%function RedBlaze%(Anchura,Angulo)

function [H]=Red_Blaze_Paco(Anchura,sizex,sizey); 

%Anchura=23;
Angulo=1.5;

%sizex=700;
%sizey=600;

H=zeros(sizex,sizey);

Paso=256/(Anchura-1);

for i=1:Anchura
    NivelesGris(1,i)=Paso*(i-1);
end


 for j=1:sizex 
    for k=1:sizey
     
        jrot=round(j*cos(Angulo));
        krot=round(k*sin(Angulo));
        
         if k<=Anchura
           
           H(j,k)=NivelesGris(1,k);
           
         else
           l=rem(k,Anchura)+1;
           
           H(j,k)=NivelesGris(1,l);
          
       end
          
    end
end

%imagesc(H); colorbar; colormap(gray);
%A=imrotate(H,Angulo)
%imagesc(A)


NivelesGris;