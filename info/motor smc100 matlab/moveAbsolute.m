function moveAbsolute(obj_serial, desplazamiento, nMotor)
% Acciones:
%   Ejecuta un movimiento absoluto "desplazamiento" unidades.
%
% Par�metros de entrada:
%   - obj_serial: referencia al objeto serial port
%
% Devuelve: 
%   - Nada

if nargin == 2 , nMotor = 1; end % n�mero de motor por defecto

flushoutput(obj_serial); %Borrado del buffer de salida.
fprintf(obj_serial, [num2str(nMotor),'PA', num2str(desplazamiento)]);