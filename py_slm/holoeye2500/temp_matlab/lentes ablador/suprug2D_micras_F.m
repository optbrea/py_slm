% Generaci�n de una superficie rugosa plana

function [s,T,Delta,ancho,largo,h_corr] = suprug2D_micras_F(s,T,Delta,ancho,largo)

% Da la altura h_corr en micas introduciendo la rugosidad s,
% la long. de correlaci�n t y el paso Delta en micrones,
% junto con el ancho y el largo en micras

% Para la generaci�n de la superficie cil�ndrica rugosa
% se sigue el procedimiento del promedio m�vil (Ogilvy p.224)

% PAR�METROS DE LA SUPERFICIE
L_ancho=ancho/(2*Delta);
L_largo=largo/(2*Delta);

primera=1;
ultima=1;
for i=primera:ultima
    
    disp(['Superficie ' num2str(i) ' de ' num2str(ultima)]);

M=round(4*T/(sqrt(2)*Delta)); % Fija la cantidad de pesos: (2M+1)x(2M+1)

N_ancho=floor(L_ancho+M); % Fija la cantidad de alturas
                          % no correlacionadas sobre
                          % el ancho: 2Nancho+1

N_largo=floor(L_largo+M); % Fija la cantidad de alturas 
                          % no correlacionadas sobre el largo: 2Nlargo+1

% malla_h_no_corr=[2*N_ancho+1,2*N_largo+1]; % Tama�o previo de la malla de puntos o 
%                                     % nodos sobre la superficie
% 
% malla_pesos=[2*M+1,2*M+1]; % Tama�o de la malla de 
%                            % pesos sobre la superficie
% 
% malla_h_corr=[2*(N_ancho-M)+1,2*(N_largo-M)+1]; % Tama�o final de la malla de 
                                         % nodos sobre la superficie


% GENERACI�N DE LA SUPERFICIE

% Pesos para correlacionar las alturas:
[desp_ancho,desp_largo]=meshgrid(-M:M); % Desplazamientos para correlacionar
                                   % (dominio del kernel de la correlacion)
desp_ancho=desp_ancho*Delta;
desp_largo=desp_largo*Delta;

pesos=exp(-2*(desp_ancho.^2+desp_largo.^2)/(T^2)); % Pesos para la correlacion
cte_norm=sum(sum(pesos.^2));
pesos=pesos/sqrt(cte_norm); % p' q' la suma de sus cuadrados de 1

% Alturas no correlacionadas:
h_no_corr=s*randn(2*N_ancho+1,2*N_largo+1); % Alturas no correlacionadas 
                                     % con distribucion N(0,s)

% Alturas correlacionadas en el plano:
% Las alturas salen de correlacionar entre s� las h no corr 
% reemplazando una de ellas por el promedio ponderado con sus vecinas
h_corr=conv2(h_no_corr,pesos,'valid'); % Como la fcion pesos es par, las 
clear h_no_corr                        % fciones correlaci�n y convoluci�n
clear pesos                            % son equivalentes.
                                       % En en la conv 2D los pesos deben ir
                                       % 2dos en el argumento
nom=strcat('EthelExp_muestra3_Al_',num2str(i),'.mat');
save (nom,'Delta', 'T', 's', 'ancho', 'largo', 'h_corr')
end
