# !/usr/bin/env python
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------
# Name:        motor_newport.py
# Purpose:     class motor newport smc1000
#
# Author:      Luis Miguel Sanchez Brea
#
# Creation:    15/10/2018
# History:     15/10/2018 functions from matlab
#
# Dependences:
# License:     UCM/AOCG
# ----------------------------------------------------------------------
"""
This module is used for movement of motors Newport

"""

import time
import serial
from serial.tools import list_ports
import matplotlib.pyplot as plt


def connected_serial_ports(verbose=True):
    list = list_ports.comports()
    connected = []
    for element in list:
        connected.append(element.device)
    if verbose is True:
        print("Connected COM ports: " + str(connected))
    return connected


def show_image(image, verbose=True):
    plt.figure()
    IDimage = plt.imshow(
        image, interpolation='bilinear', aspect='auto', origin='lower')
    plt.axis('off')
    plt.axis('equal')
    IDimage.set_cmap("gray")
    if verbose is True:
        print("max={}. min={}, shape = {}".format(image.max(), image.min(),
                                                  image.shape))

    return IDimage
