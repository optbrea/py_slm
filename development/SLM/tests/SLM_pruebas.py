# !/usr/bin/env python3
# -*- coding: utf-8 -*-
# ------------------------------------
# Autor:    Luis Miguel Sanchez Brea
# Fecha     2012/11/01 (version 1.0)
# Licencia: copyright
# Objetivo: examples para la clase SLM
# -------------------------------------

from rutas import *
from SLM import *


def mask_amplitude_binaria():
    miniPixel = 4
    
    SLM0 = SLM(kind=kopin, wavelength=0.6328, miniPixel=miniPixel, formaPixel=formaPixel)

    x=SLM0.mask_ideal.x
    y=SLM0.mask_ideal.y
    wavelength=SLM0.mask_ideal.wavelength
    
    mask = Scalar_mask_XY(x, y, wavelength)
    mask.ring(r0=(0 * um, 0 * um), radius1=(500 * um, 500 * um), radius2=(250 * um, 250 * um), angle=0 * degrees)
    SLM0.cargarMascara(mask)

    z0=40*mm

    kind_visualizacion=(0,0,0,1)
    
    if kind_visualizacion[0]==True:
        #propagación de la máscara con número de datos igual al de pixeles
        SLM0.draw("mask_ideal")
        u0 = SLM0.mask_ideal.RS(z=z0, new_field=True)
        u0.draw(title="mask ideal")
        draw_varios_fields((SLM0.mask_ideal, u0),titulos=('mask', 'propagacion'), \
            title='mask_ideal', figsize=(12,6))
            
    if kind_visualizacion[1]==True:
        SLM0.draw("mask_enviar")
        u1 = SLM0.mask_enviar.RS(z=z0, new_field=True)
        u1.draw(title="mask_enviar")
        draw_varios_fields((SLM0.mask_enviar, u1),titulos=('mask', 'propagacion'), \
            title='mask_enviar', figsize=(12,6))

    if kind_visualizacion[2]==True:
        #propagación de la máscara con número de datos expandido
        #NO se consideran los edges del pixel (sobre todo en amplitude)
        SLM0.draw("simulacion_ideal")
        u2 = SLM0.simulacion_ideal.RS(z=z0, new_field=True)
        u2.draw(title="simulacion ideal")
        draw_varios_fields((SLM0.simulacion_ideal, u2),titulos=('mask', 'propagacion'), \
            title='simulación ideal', figsize=(12,6))

    if kind_visualizacion[3]==True:
        #propagación de la máscara con número de datos expandido
        #se consideran los edges del pixel (sobre todo en amplitude)
        #se considera la transmisión del LCD, no solo el level del pixel.
        SLM0.draw("simulacion_real")
        u3 = SLM0.simulacion_real.RS(z=z0, new_field=True)
        u3.draw(title=r"simulacion real")
        draw_varios_fields((SLM0.simulacion_real, u3),titulos=('mask', 'propagacion'), \
            title='simulacion_real', figsize=(12,6))

def test_SLM():
    mask_amplitude_binaria()
    #prueba()

if __name__ == '__main__':
    test_SLM()
    plt.show()
