# !/usr/bin/env python3
# -*- coding: utf-8 -*-
# ------------------------------------
# Autor:    Luis Miguel Sanchez Brea
# Fecha     2012/11/01 (version 1.0)
# Licencia: copyright
# Objetivo: examples para la comparar el modelo teórico con la clase experimental
# -------------------------------------


from rutas import *
import LCD
from LCD import *


modeloBurmanFase1 = {
        'gl': sp.array(range(256)),
        'alfa': -79.5 * degrees,
        'beta': beta_example(gl=gl, betaMax=100 * degrees, betaMin=5 * degrees),
        'psi': -1.8*degrees,
        'delta': delta_example(gl=gl, deltaMax=20 * degrees, deltaMin=2.5 * degrees),
        'haz_incidente': circular_light('d'),
        'wavelength': 0.6238 * um,
        'theta1': 72*degrees,
        'theta2': 24*degrees,
        }

modeloBurmanFase2 = {
        'gl': sp.array(range(256)),
        'alfa': -79.5 * degrees,
        'beta': beta_example(gl=gl, betaMax=100 * degrees, betaMin=5 * degrees),
        'psi': -1.8*degrees,
        'delta': delta_example(gl=gl, deltaMax=20 * degrees, deltaMin=2.5 * degrees),
        'haz_incidente': circular_light('d'),
        'wavelength': 0.6238 * um,
        'theta1': 35*degrees,
        'theta2': 60*degrees,
        }

modeloBurmanAmplitud = {
        'gl': sp.array(range(256)),
        'alfa': -79.5 * degrees,
        'beta': beta_example(gl=gl, betaMax=100 * degrees, betaMin=5 * degrees),
        'psi': -1.8*degrees,
        'delta': delta_example(gl=gl, deltaMax=20 * degrees, deltaMin=2.5 * degrees),
        'haz_incidente': circular_light('d'),
        'wavelength': 0.6238 * um,
        'theta1': 107*degrees,
        'theta2': 79*degrees,
        }

modeloBurmanBinario = {
        'gl': sp.array(range(256)),
        'alfa': -79.5 * degrees,
        'beta': beta_example(gl=gl, betaMax=100 * degrees, betaMin=5 * degrees),
        'psi': -1.8*degrees,
        'delta': delta_example(gl=gl, deltaMax=20 * degrees, deltaMin=2.5 * degrees),
        'haz_incidente': circular_light('d'),
        'wavelength': 0.6238 * um,
        'theta1': 35*degrees,
        'theta2': 151*degrees,
        }



def test_barridoPolarizersintensity1():
    LCD0 = LCD.LCD(modelo=modeloBurmanFase1)

    theta1 = sp.linspace(0, sp.pi, 100)
    theta2 = sp.linspace(0, sp.pi, 100)

    LCD0.gl0 = 0
    intesities = LCD0.barridoPolarizersintensity(theta1=theta1, theta2=theta2, modelo='DC', tecnica='matrix')

    title = "%d" % LCD0.gl0

    intesities = dataMatrix(x=theta1*180/sp.pi, y=theta2*180/sp.pi, matrix=intesities, xtext=r"$\theta_1$", ytext=r"$\theta_2$", title=title)
    intesities.draw()



def test_generarmatrixPotencias():
    LCD0 = LCD.LCD(modelo=modeloBurmanFase1)
    
    
    theta1 = sp.linspace(0, sp.pi, 100)
    theta2 = sp.linspace(0, sp.pi, 100)
    LCD0.gl0=0
    intesities = LCD0.barridoPolarizersintensity(theta1=theta1, theta2=theta2, modelo='DC', tecnica='matrix')
    intesities = dataMatrix(x=theta1*180/sp.pi, y=theta2*180/sp.pi, matrix=intesities, xtext="", ytext="", title='simulacion Voltaje LCD')
    intesities.add_noise(sc=0.1, I=0)
    intesities.guardarMAT(filename='simulacionmatrixExperimental')
    intesities.draw()
    

def test_comparacion():
    LCD0 = LCD.LCD(modelo=modeloBurmanFase1)
    filename='simulacionmatrixExperimental.mat'

    c1=LCD_comparar_teoria_experimento(LCD_teorico=LCD0, graficaExperimental=filename)

    c1.LCD_teorico.gl0=0
    error=c1.error()
    print error 

    c1.LCD_teorico.gl0=128
    error=c1.error()
    
    print error 


if __name__ == '__main__':

    test_barridoPolarizersintensity1()
    test_generarmatrixPotencias()
    test_comparacion()
    plt.show()
