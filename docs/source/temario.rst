Configuration
======================================

.. toctree::
   :maxdepth: 4
   :numbered:
   :glob:


   config_slm.ipynb

Sending images to SLM
======================================

.. toctree::
   :maxdepth: 4
   :numbered:
   :glob:

   images/*

Motor movement
======================================

.. toctree::
   :maxdepth: 4
   :numbered:
   :glob:

   motor/*

Camera capture
======================================

.. toctree::
   :maxdepth: 4
   :numbered:
   :glob:

   camera/*
