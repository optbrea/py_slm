function homeSearch(obj_serial, nMotor)
% Acciones:
%   Ejecuta home search.
%
% Par�metros de entrada:
%   - obj_serial: referencia al objeto serial port
%
% Devuelve: 
%   - Nada

if nargin == 1 , nMotor = 1; end % n�mero de motor por defecto
flushoutput(obj_serial); %Borrado del buffer de salida.
fprintf(obj_serial, [num2str(nMotor),'OR'])