#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass scrbook
\begin_preamble
\usepackage[T1]{fontenc}
\usepackage{times}

\usepackage{listings}
\usepackage{color}

 
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.6,0.6,0.6}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\definecolor{grisecillo}{rgb}{0.95,0.95,0.95}
 
\lstset{ %
  language=Python,                % El lenguaje del código
  basicstyle=\footnotesize,       % El tamaño de las fuentes que son usadas para el código.
  numbers=left,                   % Localización de la línea numerada
  numberstyle=\tiny\color{gray},  % Estilo de la línea numerada
  stepnumber=1,                   % Paso entre la línea numerada
  numbersep=5pt,                  % Tamaño de la fuente de los números
  backgroundcolor=\color{grisecillo},  % Elección del color del fondo. Debe añadirse \usepackage{color}
  showspaces=false,               % Mostrar espacios añadiendo la indicación
  showstringspaces=false,         % Subrayar espacios dentro de cadenas
  showtabs=false,                 % Mostrar tabulaciones dentro de cadenas añadiendo indicación
  frame=single,                   % Añadir un marco alrededor del código
  rulecolor=\color{black},        % Si no está puesto, el marco de color puede cambiarse en line-breaks within not-black text (e.g. commens (green here))
  tabsize=4,                      % Establece por defecto el tamaño de tabulación a 2 espacios
  captionpos=b,                   % Establece el título de la posición hacia abajo
  breaklines=true,                % Establece los saltos de linea automaticos
  breakatwhitespace=false,        % Establece si los saltos de linea automáticos deben suceder dentro del espacio en blanco
  title=\lstname,                 % Muestra el nombre del archivo de los archivos incluidos con \lstinputlisting;
                                  % also try caption instead of title
  keywordstyle=\color{blue},          % Estilo de las palabras clave
  commentstyle=\color{gray},       % Estilo de los comentarios dkgreen
  stringstyle=\color{dkgreen},          % Estilo de las cadenas literales mauve
  escapeinside={\%*}{*)},            % Si quieres añadir un comentario dentro de tu código
  morekeywords={matriz,True,False}        % Si quieres añadir mas palabras clave a las establecidas
}


% increase link area for cross-references and autoname them
\AtBeginDocument{\renewcommand{\ref}[1]{\mbox{\autoref{#1}}}}
\newlength{\abc}
\settowidth{\abc}{\space}
\AtBeginDocument{%
\addto\extrasenglish{
 \renewcommand{\equationautorefname}{\hspace{-\abc}}
 \renewcommand{\sectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsubsectionautorefname}{sec.\negthinspace}
 \renewcommand{\figureautorefname}{Fig.\negthinspace}
 \renewcommand{\tableautorefname}{Tab.\negthinspace}
}
}

% in case somebody want to have the label "equation"
%\renewcommand{\eqref}[1]{equation~(\negthinspace\autoref{#1})}

% that links to image floats jumps to the beginning
% of the float and not to its caption
\usepackage[figure]{hypcap}

% the pages of the TOC is numbered roman
% and a pdf-bookmark for the TOC is added
\let\myTOC\tableofcontents
\renewcommand\tableofcontents{%
  \frontmatter
  \pdfbookmark[1]{\contentsname}{}
  \myTOC
  \mainmatter }

% make caption labels bold
\setkomafont{captionlabel}{\bfseries}
\setcapindent{1em}

% enable calculations
\usepackage{calc}

% fancy page header/footer settings
\renewcommand{\chaptermark}[1]{\markboth{#1}{#1}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

% increase the bottom float placement fraction
\renewcommand{\bottomfraction}{0.5}

% avoid that floats are placed above its sections
\let\mySection\section\renewcommand{\section}{\suppressfloats[t]\mySection}
\end_preamble
\options intoc,index=totoc,BCOR10mm,captions=tableheading
\use_default_options true
\begin_modules
customHeadersFooters
\end_modules
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "lmodern" "default"
\font_sans "lmss" "default"
\font_typewriter "lmtt" "default"
\font_math "auto" "auto"
\font_default_family rmdefault
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 12
\spacing onehalf
\use_hyperref true
\pdf_bookmarks true
\pdf_bookmarksnumbered true
\pdf_bookmarksopen true
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref section
\pdf_pdfusetitle true
\pdf_quoted_options "pdfpagelayout=OneColumn, pdfnewwindow=true, pdfstartview=XYZ, plainpages=false"
\papersize a4paper
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine natbib
\cite_engine_type numerical
\biblio_style plainnat
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date true
\justification true
\use_refstyle 0
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 2
\paragraph_separation skip
\defskip medskip
\is_math_indent 1
\math_indentation default
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 2
\paperpagestyle fancy
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
\begin_inset Argument 1
status open

\begin_layout Plain Layout
Software
\end_layout

\end_inset

Software
\end_layout

\end_body
\end_document
