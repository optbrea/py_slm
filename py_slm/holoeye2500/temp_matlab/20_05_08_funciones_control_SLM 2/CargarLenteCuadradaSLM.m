%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FUNCI�N PARA CARGAR EL MAPA DE FASE DE JAVIER DE LA LENTE DE FRESNEL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [matriz_mapa_fase,inicio_x,fin_x,inicio_y,fin_y,ID]=CargarLenteCuadradaSLM(mapafase,coordenadas_pant_virtual,ID)

% 22/4/08 Funciona
% 19/5/08 cambio sobre c�mo rellenar la matriz de ceros (LM)

% size_SLM_x es el tama�o del modulador en x: 1024
% size_SLM_x es el tama�o del modulador en y: 768
% size_lente_x 240
% size_lente_y 240
% coordenadas_pant_virtual: [-1 -1025 1024 768]
% coordenadas_pant_normal: [1 1 1024 768]
% En la variable ID, el asa se crea colocando '' en su lugar.

% LA FUNCI�N VALE PARA CUALQUIER TAMA�O DE LA LENTE DE FRESNEL 

size_SLM_x=1024;
size_SLM_y=768;
[size_lente_x,size_lente_y]=size(mapafase);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ESTA FUNCI�N CARGA EL MAPA DE FASE DE JAVIER EXTENDI�NDOLO A
% TRANSMITANCIA 1 FUERA DE LA LENTE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% CREO LA MATRIZ EXTENDIDA

%load 'mapafase.mat'; % Fase de 240x240

centro_x_ventana=(size_SLM_x/2)-1; % Centros en x,y.
centro_y_ventana=(size_SLM_y/2)-1;

inicio_x=centro_x_ventana-(size_lente_x/2); % Defino los tama�os de la ventana y supongo que la lente tiene un n�mero de p�xeles par
fin_x=centro_x_ventana+(size_lente_x/2)-1; % Hay que incluir al cero de la ventana
inicio_y=centro_y_ventana-(size_lente_y/2);
fin_y=centro_y_ventana+(size_lente_y/2)-1;

matriz_mapa_fase=zeros(size_SLM_x,size_SLM_y,'uint8');                  %la matriz inicial es todo ceros
matriz_mapa_fase(inicio_x:fin_x,inicio_y:fin_y)=uint8(mapafase);        %relleno la matriz en el centro con la de Javier.


% CARGO LA MATRIZ EN LA PANTALLA VIRTUAL

if not(isempty(coordenadas_pant_virtual))
    if isempty(ID)
      %  ID=figure;
    else
     %   figure(ID);
    end

    ID=imshow(matriz_mapa_fase');
  %  set(gca,'Position',coordenadas_pant_virtual);   % [left bottom width height]
  % set(gcf,'Position',coordenadas_pant_virtual);   % [left bottom width height]
%     axis equal
%     axis square
%     refresh; drawnow
end
