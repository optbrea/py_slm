%% simulaci�n de lentes por ablaci�n sobre fleje met�lico
% 090517 Alvarez R�os / Salgado Remacha



um=1;
mm=1000*um;
grados=pi/180;

parametros.lambda=.5*um;
parametros.Nx=256;
parametros.Ny=256;
tmptamano=250*um;
parametros.ejes='';

k=2*pi/parametros.lambda;

parametros.sx=linspace(-tmptamano/2,tmptamano/2,parametros.Nx); %plano de la mascara de entrada
parametros.x=linspace(-tmptamano/2,tmptamano/2,parametros.Nx);  %plano de salida
parametros.sy=linspace(-tmptamano/2,tmptamano/2,parametros.Ny); %plano de la mascara de entrada
parametros.y=linspace(-tmptamano/2,tmptamano/2,parametros.Ny);  %plano de salida



k=2*pi./parametros.lambda;

parametros.ID=preparardibujo;
parametros.amplificacion=1;




%% objeto
    def_lente.x0=0*um;
    def_lente.y0=0*um;
    def_lente.foco=5*mm;
    def_lente.diametro=0.25*mm;
    def_lente.amplitud=1;  %amplitud 1 fase 0

%% haz de iluminaci�n
% def_haz_plano.A=1;
% def_haz_plano.theta=0*grados;
% def_haz_plano.fi=0*grados;

%% Definimos la rugosidad de la zona ablacionada

def_rugosidadAbl.sigma=.0025*um;   % desviacion estandarpositivo convergente
def_rugosidadAbl.T=15*um;%longitud de correlaci�n

def_rugosidadAbl.modo='reflexion'; %o reflexi�n
def_rugosidadAbl.indiceRefraccion=1;                      %para transmisi�n

[parametros.mascaraRugAbl, alturasAbl]= rugosidad2D(parametros,def_rugosidadAbl);
%% Definimos la rugosidad del fleje
def_rugosidadFleje.sigma=2*um;   %  desviacion estandar positivo convergente
def_rugosidadFleje.T=1*um; %longitud de correlaci�n 

def_rugosidadFleje.modo='reflexion'; %o reflexi�n
def_rugosidadFleje.indiceRefraccion=1;                      %para transmisi�n

[parametros.mascaraRugFleje, alturasFleje]= rugosidad2D(parametros,def_rugosidadFleje);
parametros.iluminacion=1;

%% definimos la mascara para el modulador

lente=lente_fresnel_2D(parametros,def_lente);

parametros.mascara = lente.*parametros.mascaraRugFleje+(1-lente).*parametros.mascaraRugAbl;
parametros.u0=parametros.mascara.*parametros.iluminacion;
ID=dibujarMascara2D(parametros,[0 1 0 0]);

 parametros.z=linspace(0.8*def_lente.foco,1.2*def_lente.foco,20);

 
 parametros=propagacion_2D(parametros);

figure; imagesc(255*mat2gray(angle(parametros.mascara))); colorbar; axis square;
corteTransversal2D(parametros,0)