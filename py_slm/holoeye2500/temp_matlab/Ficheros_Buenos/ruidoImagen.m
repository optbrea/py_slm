function [ruido,diferencia,SN,ruidoEspacial]=ruidoImagen(I1,dibujar)

if not(exist('dibujar'))
    dibujar=0;
end


I1=I1(:,:,1);
longitud=min(size(I1));
longVariograma=30;

perfil=improfile(I1,[1:longitud],[1:longitud]);
xvariogr=[1:longVariograma]';

Variogr=Variograma_1Dim_y_Nvar(perfil,xvariogr,dibujar);
P=polyfit(xvariogr,Variogr,4);
hold on
estimacionVariograma=sqrt(polyval(P,xvariogr));
maximo=max(estimacionVariograma);

if dibujar==1
plot(xvariogr,estimacionVariograma)
end


ruidoEspacial=std(perfil); %ruido calculado sin variograma;
ruido=sqrt(P(end));         %ruido calculado con variograma;
diferencia=maximo-ruido;    %se�al
SN=diferencia./ruido;       %SN