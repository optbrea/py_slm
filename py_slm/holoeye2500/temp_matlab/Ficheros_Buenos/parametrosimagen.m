function ueye=parametrosimagen(ueye,dibujarruido)


tic
[ruido,diferencia,SN]=ruidoImagen(ueye.imagen.I,dibujarruido);
ueye.imagen.ruido=ruido;
ueye.imagen.SN=SN;
[N,posx,posy]=pixelesSaturados(ueye.imagen.I);
porcentajesaturados=N/(ueye.param.ImageSize(1).*ueye.param.ImageSize(2))*100;
ueye.imagen.saturados=porcentajesaturados;
toc
