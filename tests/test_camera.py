# -*- coding: utf-8 -*-

import ctypes as C
import time
from pprint import pprint

import cv2
import numpy as np

import py_slm.camera.tisgrabber as IC
from diffractio import degrees, mm, np, plt, sp, um
from diffractio.scalar_masks_XY import Scalar_mask_XY
from py_slm.py_slm import SLM

lWidth = C.c_long()
lHeight = C.c_long()
iBitsPerPixel = C.c_int()
COLORFORMAT = C.c_int()

# Create the camera object.
Camera = IC.TIS_CAM()

# List availabe devices as uniqe names. This is a combination of camera name and serial number
Devices = Camera.GetDevices()
for i in range(len(Devices)):
    print(str(i) + " : " + str(Devices[i]))

Camera.open("DMx 72BUC02 14210296")
# Set a video format
Camera.SetVideoFormat("Y800 (2592x1944)")
# Set a frame rate of 30 frames per second
Camera.SetFrameRate(5)

Camera.StartLive(1)

Camera.SnapImage()
# Get the image
image = Camera.GetImage()

plt.figure()
plt.imshow(image)

Camera.SetPropertySwitch("Gain", "Auto", 0)
Camera.SetPropertyValue("Gain", "Value", 10)

Camera.SnapImage()
# Get the image
image2 = Camera.GetImage()

plt.figure()
plt.imshow(image)

Camera.StopLive()
plt.show()
