function obj_serial = initSerialSMC100(Npuerto)
% Acci�n: Esta funci�n crea un objeto serial port, configura sus par�metros  
%       y finalmente establece la conexi�n.
%
% Devuelve: referencia al objeto serie creado

obj_serial= serial(Npuerto); %Declaraci�n del puerto serie usado en PC  
obj_serial.baudrate= 57600;
set(obj_serial,'DataBits',8,'Parity','none','StopBits',1); % 8-N-1
obj_serial.FlowControl = 'software'; % control de flujo Xon/Xoff
obj_serial.Name= 'serial-SMC100';  % Nombramos al objeto serial creado
obj_serial.Terminator = 'CR/LF'; % Ponemos ACK como terminador en Rx
obj_serial.OutputBufferSize= 30; %tama�o en bytes del buffer de salida
obj_serial.InputBufferSize= 30; %tama�o en bytes del buffer de entrada
obj_serial.ReadAsyncMode= 'continuous'; %lectura continua del puerto serie
obj_serial.Timeout= 5; % tiempo en s antes de dar una operaci�n de lectura 
%                       o escritura como err�nea por timeout

fopen(obj_serial); %Establecemos la conexi�n con el puerto serie
