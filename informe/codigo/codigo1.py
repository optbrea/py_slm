import scipy as sp
import matplotlib.pyplot as plt


x=sp.linspace(0, 10, 100)
y=sp.sin(x)

plt.figure()
plt.plot(x,y)
plt.show()