function setHysteresisCompensation(obj_serial, hysteresisCompensation, nMotor)
% Acciones:
%   Imponemos la velocidad deseada en unidades.
%
% Par�metros de entrada:
%   - obj_serial: referencia al objeto serial port
%
% Devuelve: 
%   - Nada

if nargin == 2 , nMotor = 1; end % n�mero de motor por defecto

flushoutput(obj_serial); %Borrado del buffer de salida.
fprintf(obj_serial, [num2str(nMotor),'BH', num2str(hysteresisCompensation)]);