# !/usr/bin/env python
# -*- coding: utf-8 -*-


import time
from pprint import pprint
import cv2
import matplotlib.image as mpimg
import screeninfo
from diffractio import degrees, mm, np, plt, sp, um
from diffractio.scalar_masks_XY import Scalar_mask_XY
from py_slm.config import CONF_HOLOEYE2500, CONF_PACKARD_BELL, CONF_IMAGING_SOURCE
from py_slm.py_slm import SLM

ms = 1.
seconds = 1000 * ms


"""
Conocer los monitores disponibles. El SLM se lee como un segundo monitor al
que le enviaremos la imagen, por lo que con el siguiente código deberíamos
obtener la información de ambos monitores.
"""

screeninfo.get_monitors()

"Llamamos a la clase SLM"
slm = SLM(config_SLM=CONF_HOLOEYE2500, config_Camera=CONF_IMAGING_SOURCE)

"""
Obtenemos los parámetros para la configuración de la cámara.
Estos están ya en la clase de SLM
"""
x0 = slm.x0
y0 = slm.y0
wavelength = slm.wavelength
print(x0.shape, y0.shape)
slm.camera1.StartLive()
slm.camera1.SetPropertySwitch("Gain", "Auto", 0)
slm.camera1.SetPropertyValue("Gain", "Value", 10)
slm.get_background()
slm.show_background()
plt.show()
"""
Introducimos la máscara que queremos reproducir en el SLM. Para ello usaremos
las clases de Scalar_mask_XY y Ejercicios_TFG.
Empezaremos con un círculo simple.
"""

radius = (1000 * mm, 1000 * mm)
t1 = Scalar_mask_XY(x=x0, y=y0, wavelength=wavelength)
t1.lens(r0=(0, 0), radius=radius, angle=0, mask=True)

"Creamos la imagen a partir de la máscara creada."
slm.mask_to_rawImage(mask_XY=t1, kind='intensity', normalize=True)

"Enviamos la imagen al SLM."
slm.send_image_screen(id_screen=1, verbose=False)

"""
El problema reside en que al poner el time.sleep(), la funcionsend_image_screen
entra en pausa y no se ejecuta hasta que acabe el .sleep()
"""

cv2.waitKey(500)
imagen1 = slm.acquire_image(draw=True,
                            remove_background=False,
                            filename='imagen_camara.png',
                            is_closed=True)
img_c = mpimg.imread('imagen_camara.png')
cv2.waitKey(500)
plt.imshow(img_c)
cv2.waitKey(500)
plt.show()
