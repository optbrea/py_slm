function ueye=getUeye(ueyeCtrl,ueye)
buf=0;
    

%% Informacion sobre la camara
    ueye.info.ID=GetCameraID(ueyeCtrl);
    ueye.info.Type=GetCameraType(ueyeCtrl);
    ueye.info.Status=GetCameraStatus(ueyeCtrl,0);  %0, 1 o 9
    ueye.info.Version=GetCameraVersion(ueyeCtrl);
    ueye.info.ControlVersion=GetControlVersion(ueyeCtrl);
    ueye.info.DLLVersion=GetDLLVersion(ueyeCtrl);
    ueye.info.SensorType=GetSensorType(ueyeCtrl);
    ueye.info.SerialNumber=GetSerialNumber(ueyeCtrl);
    
%%  Informacion sobre la imagen
 ueye.param.Brightness=GetBrightness(ueyeCtrl);
 ueye.param.Contrast=GetContrast(ueyeCtrl) ;
 %ueye.param.Contrastdd=GetColorCorrection(ueyeCtrl);
 ueye.param.ExposureTime=GetExposureTime(ueyeCtrl);   %en milisegundos
 ueye.param.ExternalTrigger=GetExternalTrigger(ueyeCtrl);
 ueye.param.FramesPerSecond=GetFramesPerSecond(ueyeCtrl);
 ueye.param.FrameRate=GetFrameRate(ueyeCtrl);
 ueye.param.Gamma=GetGamma(ueyeCtrl);   % multiplicado por 100
 %ueye.param.HardwareGain=GetHardwareGain(ueyeCtrl);
 [ret,x,y]=GetImagePosition(ueyeCtrl,buf,buf);
 ueye.param.ImagePosition=[x,y];
 [ret,x,y]=GetImageSize(ueyeCtrl,buf,buf);
 ueye.param.ImageSize=[x,y];
 ueye.param.WhiteBalance=GetWhiteBalance(ueyeCtrl);
 ueye.param.EdgeEnhancement=GetEdgeEnhancement(ueyeCtrl);  %0 disabled - 1 strong - 2 weak
 ueye.param.PixelClock=GetPixelClock(ueyeCtrl);  %MHz
 ueye.param.Offset=GetBlOffset(ueyeCtrl);   %valor de offset;

 
 
ueye.Ctrl.ExternalTrigger=GetExternalTrigger(ueyeCtrl);
ueye.Ctrl.HardwareBPCEnabled=IsHardwareBPCEnabled(ueyeCtrl); %Bad Pixel Correction
ueye.Ctrl.SoftwareBPCEnabled=IsSoftwareBPCEnabled(ueyeCtrl); %Bad Pixel Correction

ueye.Ctrl.BoardConnected=IsMemoryBoardConnected(ueyeCtrl);
ueye.Ctrl.SoftwareBPCEnabled=IsSoftwareBPCEnabled(ueyeCtrl); %Bad pixel correction
ueye.Ctrl.VideoFinish=IsVideoFinish(ueyeCtrl);
ueye.Ctrl.SubSampling=GetSubSampling(ueyeCtrl);
ueye.Ctrl.RenderMode=GetRenderMode(ueyeCtrl);
 
 ueye.error.ID=GetError(ueyeCtrl);
 ueye.error.text=GetErrorText(ueyeCtrl);
 
 
 %mejorar
 [a,b,c,d]=InquireImageMem(ueyeCtrl,buf,buf,buf,buf);
 ueye.imagen.ancho=b;
 ueye.imagen.alto=c;
 ueye.imagen.bits=d;
 ueye.imagen.pitch=a;
 