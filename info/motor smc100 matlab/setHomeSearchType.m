function setHomeSearchType(obj_serial, homeSearchType, nMotor)
% Acciones:
%   Define el tipo de home search a ejecutar.
%
% Par�metros de entrada:
%   - obj_serial: referencia al objeto serial port
%
% Devuelve: 
%   - Nada

if nargin == 2 , nMotor = 1; end % n�mero de motor por defecto

flushoutput(obj_serial); %Borrado del buffer de salida.
fprintf(obj_serial, [num2str(nMotor),'HT', num2str(homeSearchType)]);