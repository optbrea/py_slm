function ueye=dibujarimagen(ueyeCtrl,ueye,capturar,ID)

if capturar==1;
    ueye=captura(ueyeCtrl,ueye);
end

if nargin==3
    if isempty(ueye.imagen.ID)
        ueye.imagen.ID=0;
    end
    ID=ueye.imagen.ID;
end

if ID==0
    ID='';
end

if isempty(ueye.imagen.x)
    ID=imagen(ueye.imagen.I,gray,[1:ueye.imagen.ancho],[1:ueye.imagen.alto],ueye.imagen.udx,ueye.imagen.udy,'',ID);
else
       ID=imagen(ueye.imagen.I,gray,ueye.imagen.x,ueye.imagen.y,ueye.imagen.udx,ueye.imagen.udy,'',ID);
end


ueye.imagen.ID=ID;