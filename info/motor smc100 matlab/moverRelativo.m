function moverRelativo(obj_serial,destino,velocidad)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% F. J. Salgado-Remacha
% 12/01/10
% moverRelativo(obj_serial,destino,velocidad)
% hace un movimiento relativo hasta la posicion "destino" a una velocidad "velocidad".
% Hasta que no alcanza el destino no sale del programa
% si no se introduce el par��ametro "velocidad", toma la velocidad por
% defecto
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



if nargin == 2 ,  
moveRelative(obj_serial, destino); %moveAbsolute(obj_serial, desplazamiento)
velocidad=getVelocity(obj_serial);
end % velocidad por defecto

setVelocity(obj_serial, velocidad); %setVelocity(obj_serial, velocidad)
moveRelative(obj_serial, destino); %moveAbsolute(obj_serial, desplazamiento)


%% compruebo que el movimiento ha acabado
posActual=(getCurrentPosition(obj_serial));
    
    
    while abs(posActual-destino)>=0.0001
       pause(0.1);
       posActual=(getCurrentPosition(obj_serial));
       
    end