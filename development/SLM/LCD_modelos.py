# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
FUNCIONES DE EJEMPLO PARA SIMULAR
"""

from scipy import cos, sin
from scipy.interpolate.interpolate import interp1d

from diffractio import degrees, mm, np, sp, um
from py_pol.jones_matrix import *
from py_pol.jones_vector import *


def beta_example(gl, betaMax=100 * degrees, betaMin=5 * degrees):
    """ kind de variación  de beta con el level de gris.
    Esta gráfica debería ser experimental, pero de momento me baso en la página 57 (Fig. 2.22)
    - betaMax: level máximo de ángulo
    - betaMin: level mínimo de ángulo
    """
    b = (betaMin + betaMax) / 2 + (betaMax - betaMin) * cos(sp.pi * gl /
                                                            (gl.max())) / 2
    return b


def delta_example(gl, deltaMax=20 * degrees, deltaMin=5 * degrees):
    """ kind de variación  de delta con el level de gris.
    Esta gráfica debería ser experimental, pero de momentome baso en la página 58 (Fig. 2.23)
    - deltaMax: level máximo de ángulo
    - deltaMin: level mínimo de ángulo
    """
    delta = deltaMin + (deltaMax - deltaMin) * sin(sp.pi * gl /
                                                   (1.5 * gl.max()))**2

    return delta


"""
MODELO PARA PAPER RARO DE AMPLITUD-FASE CON REDES
"""

modeloLM1 = {
    'gl':
    sp.array(range(256)),
    'alfa':
    90 * degrees,
    'beta':
    beta_example(
        gl=sp.array(range(256)), betaMax=360 * degrees, betaMin=0 * degrees),
    'psi':
    0 * degrees,
    'delta':
    delta_example(
        gl=sp.array(range(256)), deltaMax=45 * degrees, deltaMin=0 * degrees),
    'haz_incidente':
    circular_light('d'),
    'wavelength':
    0.6238 * um,
    'theta1':
    0 * degrees,
    'theta2':
    90 * degrees,
}
"""
MODELOS DE LA TESIS DE BURMAN
"""
modeloBurmanFase1 = {
    'gl':
    sp.array(range(256)),
    'alfa':
    -79.5 * degrees,
    'beta':
    beta_example(
        gl=sp.array(range(256)), betaMax=100 * degrees, betaMin=5 * degrees),
    'psi':
    -1.8 * degrees,
    'delta':
    delta_example(
        gl=sp.array(range(256)), deltaMax=20 * degrees,
        deltaMin=2.5 * degrees),
    'haz_incidente':
    circular_light('d'),
    'wavelength':
    0.6238 * um,
    'theta1':
    72 * degrees,
    'theta2':
    24 * degrees,
}

modeloBurmanFase2 = {
    'gl':
    sp.array(range(256)),
    'alfa':
    -79.5 * degrees,
    'beta':
    beta_example(
        gl=sp.array(range(256)), betaMax=100 * degrees, betaMin=5 * degrees),
    'psi':
    -1.8 * degrees,
    'delta':
    delta_example(
        gl=sp.array(range(256)), deltaMax=20 * degrees,
        deltaMin=2.5 * degrees),
    'haz_incidente':
    circular_light('d'),
    'wavelength':
    0.6238 * um,
    'theta1':
    35 * degrees,
    'theta2':
    60 * degrees,
}

modeloBurmanAmplitud = {
    'gl':
    sp.array(range(256)),
    'alfa':
    -79.5 * degrees,
    'beta':
    beta_example(
        gl=sp.array(range(256)), betaMax=100 * degrees, betaMin=5 * degrees),
    'psi':
    -1.8 * degrees,
    'delta':
    delta_example(
        gl=sp.array(range(256)), deltaMax=20 * degrees,
        deltaMin=2.5 * degrees),
    'haz_incidente':
    circular_light('d'),
    'wavelength':
    0.6238 * um,
    'theta1':
    107 * degrees,
    'theta2':
    79 * degrees,
}

modeloBurmanBinario = {
    'gl':
    sp.array(range(256)),
    'alfa':
    -79.5 * degrees,
    'beta':
    beta_example(
        gl=sp.array(range(256)), betaMax=100 * degrees, betaMin=5 * degrees),
    'psi':
    -1.8 * degrees,
    'delta':
    delta_example(
        gl=sp.array(range(256)), deltaMax=20 * degrees,
        deltaMin=2.5 * degrees),
    'haz_incidente':
    circular_light('d'),
    'wavelength':
    0.6238 * um,
    'theta1':
    35 * degrees,
    'theta2':
    151 * degrees,
}
"""modelo experimental de modulador de proyector SONY"""
alfa = -90.7832688192 * degrees
beta_maxima = 134.540877676 * degrees
beta0 = sp.array([
    -14.75115406, -15.71686778, -19.99936453, -24.75304376, -30.97099113,
    -40.01031685, -49.44003965, -62.81038974, -77.923332, -91.36725938,
    -105.11971535, -111.51588345, -117.02710469, -129.0265514, -131.70797705,
    -131.84139714
]) * degrees
psi = 32.5008547753 * degrees
delta0 = sp.array([
    26.65533244, 41.85755408, 57.73401392, 64.96715132, 71.27024504,
    77.23831921, 85.12128394, 94.25451813, 105.61446543, 116.38728704,
    127.21404866, 132.87248308, 137.27601942, 149.1519894, 152.17715049,
    152.35101835
]) * degrees
error = [
    4.41090251, 0.28419206, 0.36585532, 0.33161884, 0.41375047, 0.5350761,
    0.64365905, 0.80031859, 0.87722312, 0.8363581, 0.77251774, 0.51642338,
    0.4330389, 0.58729751, 0.2515351, 0.2192197
]

gl0 = sp.linspace(0, 255, 16)
gl = range(256)
f_beta = interp1d(gl0, beta0, kind='linear')
beta_sony = f_beta(gl)

f_delta = interp1d(gl0, delta0, kind='linear')
delta_sony = f_delta(gl)
"""
plt.figure()
plt.subplot(2,1,1)
plt.plot(gl0,beta0/degrees,'ko')
plt.plot(gl,beta_sony/degrees,'r')
plt.legend(['data','linear'], loc='best')

plt.subplot(2,1,2)
plt.plot(gl0,delta0/degrees,'ko')
plt.plot(gl,delta_sony/degrees,'r')
plt.legend(['data','linear'], loc='best')
"""

modelo_experimental_sony = {
    'gl': sp.array(range(256)),
    'alfa': -90.7832688192 * degrees,
    'beta': beta_sony,
    'psi': 32.50 * degrees,
    'delta': delta_sony,
    'haz_incidente': circular_light('d'),
    'wavelength': 0.6328 * um,
    'theta1': 9.57 * degrees,
    'theta2': 56.27 * degrees,
}
