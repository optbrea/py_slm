






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FUNCI�N PARA CARGAR EL MAPA DE FASE DE JAVIER DE LA LENTE DE FRESNEL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [matriz_mapa_fase,inicio_x,fin_x,inicio_y,fin_y,asa_figura]=Carga_Imagen_Javier(size_SLM_x,size_SLM_y,size_lente_x,size_lente_y,coordenadas_pant_virtual)

% 22/4/08

% Funciona


% size_SLM_x es el tama�o del modulador en x: 1024
% size_SLM_x es el tama�o del modulador en y: 768
% coordenadas_pant_virtual: [-1 -1025 1024 768]  
% En la variable ID, el asa se crea colocando '' en su lugar.

% LA FUNCI�N VALE PARA CUALQUIER TAMA�O DE LA LENTE DE FRESNEL 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ESTA FUNCI�N CARGA EL MAPA DE FASE DE JAVIER EXTENDI�NDOLO A
% TRANSMITANCIA 1 FUERA DE LA LENTE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% CREO LA MATRIZ EXTENDIDA

load 'mapafase.mat'; % Fase de 240x240


centro_x_ventana=(size_SLM_x/2)-1; % Centros en x,y.
centro_y_ventana=(size_SLM_y/2)-1; 

inicio_x=centro_x_ventana-(size_lente_x/2); % Defino los tama�os de la ventana y supongo que la lente tiene un n�mero de p�xeles par
fin_x=centro_x_ventana+(size_lente_x/2)-1; % Hay que incluir al cero de la ventana
inicio_y=centro_y_ventana-(size_lente_y/2);
fin_y=centro_y_ventana+(size_lente_y/2)-1;

matriz_mapa_fase=ones(size_SLM_x,size_SLM_y,'uint8');

for i=inicio_x:fin_x           % Relleno el SLM.
    for j=inicio_y:fin_y
    
    matriz_mapa_fase(i,j)=uint8(mapafase(i-inicio_x +1,j-inicio_y +1));      
        
    end;
end;


% CARGO LA MATRIZ EN LA PANTALLA VIRTUAL

asa_figura=figure;   % Creo el asa

set(asa_figura,'Position',coordenadas_pant_virtual);   % [left bottom width height]

image(matriz_mapa_fase);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

















