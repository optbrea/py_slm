# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# ------------------------------------
# Autor:    Luis Miguel Sanchez Brea
# Fecha     2012 (version 1.0)
# Licencia: copyright, DPI2011-
# Objetivo: Módulo que simula un SLM (en principio Kopin, pero puede ser cualquiera)
# -----------------------

from diffractio import degrees, np, plt, sp, um
from diffractio.scalar_masks_XY import Scalar_mask_XY
from LCD import *

wavelength = 0.6328 * um

# Definición de los parámetros de Kopin

kopin = {
    'pixelSize': (11. * um, 11. * um),
    'numPixels': (128, 128),  # (320, 240)
    'areaActiva': (8. * um, 8. * um),
    'kind': 'amplitude',  # amplitude o phase
    'modelo': modelo_experimental_sony,
}

miniPixel = 4
formaPixel = sp.ones((miniPixel, miniPixel))

# formaPixel[0:4, :] = 0
# formaPixel[:, 0:4] = 0


class SLM(object):
    def __init__(self,
                 kind=kopin,
                 wavelength=0.6328,
                 miniPixel=miniPixel,
                 formaPixel=formaPixel):
        """
        modelo: 'kopin' diccionario con los parámetros del moduldaor,
        minipixel: número de subdivisiones que tiene un pixel en el modulador para las simulaciones
        formaPixel: maitriz del length de minipixel para simular las eliminaciones de los edges
                    si es None lo deja todo a unos (factor de forma 1)
        """

        self.param = kind
        self.miniPixel = miniPixel

        if formaPixel is None:
            self.formaPixel = sp.ones((miniPixel, miniPixel))
        else:
            self.formaPixel = formaPixel

        # tamaño del modulador
        sizeX = self.param['pixelSize'][0] * self.param['numPixels'][0]
        sizeY = self.param['pixelSize'][1] * self.param['numPixels'][1]

        # numero pixels modulador
        nx = self.param['numPixels'][0]
        ny = self.param['numPixels'][1]

        # numero pixels simulacion
        nx_sim = self.param['numPixels'][0] * miniPixel
        ny_sim = self.param['numPixels'][1] * miniPixel

        # mask
        xm = sp.linspace(-sizeX / 2, sizeX / 2, nx)
        ym = sp.linspace(-sizeY / 2, sizeY / 2, ny)

        # simulacion
        xs = sp.linspace(-sizeX / 2, sizeX / 2, nx_sim)
        ys = sp.linspace(-sizeY / 2, sizeY / 2, ny_sim)

        # máscara deseada (número complejo, según se desee)
        self.mask_ideal = Scalar_mask_XY(x=xm, y=ym, wavelength=wavelength)

        # máscara enviar a modulador (0-255)
        self.mask_enviar = Scalar_mask_XY(x=xm, y=ym, wavelength=wavelength)

        # simulación ideal sin las características del SLM (no edges, no modulacion SLM)
        self.simulacion_ideal = Scalar_mask_XY(
            x=xs, y=ys, wavelength=wavelength)

        # simulacion con las características del SLM (según la máscara deseada, pero más grande)
        self.simulacion_real = Scalar_mask_XY(
            x=xs, y=ys, wavelength=wavelength)

        LCD0 = LCD(modelo=self.param['modelo'])
        self.transmitancia = LCD0.get_fields(
            kind='u_escalar', modelo='DC', tecnica='matrix')

        print self.transmitancia

        # Todavía no se ha cargado la máscara

    def __str__(self):
        print '\nSLM:'
        print ' numPixels: ', self.mask.u.shape
        print ' length: ', self.mask.x[-1] - self.mask.x[
            0], 'um x ', self.mask.y[-1] - self.mask.y[-0], ' um'
        print 'simulacion:'
        print ' numPixels: ', self.simulacion.u.shape
        print ' length: ', self.simulacion.x[-1] - self.simulacion.x[
            -0], ' um x ', self.simulacion.y[-1] - self.simulacion.y[-0], 'um'
        print ' pixelado:\n ', self.formaPixel

    def cargarMascara(self, mask_ideal):
        # cargo la máscara que deseo enviar al modulador
        self.mask_ideal.u = mask_ideal.u
        # cargar la simulación
        self.cargarSimulacion()
        # máscara en levels de gris (0-255) que será la que se envie
        self.mask_enviar.u = mask_ideal.u  # hay que cambiar, pero aquí está el truco de la cuestión

    def cargarSimulacion(self):
        for i in range(self.miniPixel):
            for j in range(self.miniPixel):
                # creo la máscara ideal
                self.simulacion_ideal.u[i::self.miniPixel, j::self.
                                        miniPixel] = self.mask_ideal.u

                # creo la máscara real, con la forma de pixel y con las transmitancias que "PUEDO" (según self.transmitancia)
                self.simulacion_real.u[
                    i::self.miniPixel, j::self.
                    miniPixel] = self.formaPixel[i, j] * self.mask_ideal.u
                # hay que cambiar self.mask.u por lo que puedo hacer con el pixel

    def mandarSLM(self):
        """enviar la máscara al modulador"""

        pass

    def draw(self, queDibujar="mask_ideal", kind="intensity"):
        """
        queDibujar: "mask_ideal", "simulacion_ideal", "simulacion_real", "mask_enviar"
        """
        if queDibujar == "mask_ideal":
            self.mask_ideal.draw(kind)
            plt.title("mask")

        if queDibujar == "simulacion_ideal":
            self.simulacion_ideal.draw(kind)
            plt.title("simulacion_ideal")

        if queDibujar == "simulacion_real":
            self.simulacion_real.draw(kind)
            plt.title("simulacion_real")

        if queDibujar == "mask_enviar":
            self.mask_enviar.draw(kind)
            plt.title("mask_enviar")


if __name__ == '__main__':
    from SLM_tests import test_SLM
    test_SLM()
    plt.show()
