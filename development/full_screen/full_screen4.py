import pygame

pygame.init()
screen = pygame.display.set_mode((0,0),pygame.FULLSCREEN)
done = False
image1 = pygame.image.load("hexagono1.png")
image2 = pygame.image.load("hexagono2.png")
image3 = pygame.image.load("hexagono3.png")


back1 = pygame.Surface(screen.get_size())
back1 = back1.convert()
back1.blit(image1,(0,0))
back2 = pygame.Surface(screen.get_size())
back2 = back2.convert()
back2.blit(image2,(0,0))
back3 = pygame.Surface(screen.get_size())
back3 = back3.convert()
back3.blit(image3,(0,0))



while not done:
    for event in pygame.event.get():
        if event.kind == pygame.QUIT:
            done = True
        elif event.kind == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                done = True
    screen.blit(back1,(0,0))
    pygame.display.flip()
    screen.blit(back2,(0,0))
    pygame.display.flip()
    screen.blit(back3,(0,0))
    pygame.display.flip()

   
pygame.quit()
