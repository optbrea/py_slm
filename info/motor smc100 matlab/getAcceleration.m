function acceleration = getAcceleration(obj_serial, nMotor)
% Acciones:
%   Devuelve el valor de la aceleracion programada.
%
% Par�metros de entrada:
%   - obj_serial: referencia al objeto serial port
%
% Devuelve: 
%   - Nada

if nargin == 1 , nMotor = 1; end % n�mero de motor por defecto
flushoutput(obj_serial); %Borrado del buffer de salida.
flushinput(obj_serial);  %Borrado del buffer de entrada.
fprintf(obj_serial, [num2str(nMotor),'AC', '?']);

%lectura as�ncrona (bloqueante) con terminador
[respuestaStr, count] = fscanf(obj_serial);
if (count == 0)
    % lectura no v�lida, ha expirado el timeout sin recibir respuesta
    acceleration = -9999999;
else
    % hemos recibido respuesta antes de que expire el timeout
    numMotor=textscan(respuestaStr, '%f'); % leo el primer sub-string que representa un n�mero
    cabecera = length(numMotor)+3;
    acceleration = str2double(respuestaStr(cabecera: length(respuestaStr)));
end