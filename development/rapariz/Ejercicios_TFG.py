# !/usr/bin/env python
# -*- coding: utf-8 -*-

import math

import matplotlib.figure as mpfig
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from scipy.signal import fftconvolve

from diffractio import degrees, mm, um
from diffractio.scalar_fields_XY import Scalar_field_XY, draw_several_fields
from diffractio.scalar_masks_XY import Scalar_mask_XY


"""
Ejercicios relacionados con el TFG con el fin de mejorar mis habilidades con
python

"""


class Ejercicios(Scalar_field_XY):
    def __init__(self, x=None, y=None, wavelength=None, info=""):
        print("Se ha creado la clase Ejercicios")
        super(self.__class__, self).__init__(x, y, wavelength, info)

    def sinusoidal_slit(self,
                        y0=(10 * um, -10 * um),
                        amplitude=10 * um,
                        phase=0 * degrees,
                        angle=0 * degrees,
                        wavelength_b=50 * um):
        """
        This function will create a sinusoidal wave-like slit
        Args:
            y0 (float, float): y-initial positions of the wave-like borders of the slit
            amplitude (float, float): Amplitudes of the wave-like borders of the slit
            phase (float): Phase between the wave-like borders of the slit
            angle (float): Angle to be rotated the sinusoidal slit
            wavelength_b (float): wavelength of the wave-like border of the slit

        Example:

            sinusoidal_slit(y0=(10 * um, -10 * um), amplitude=(10 * um, 20 * um), phase=0 * degrees,
                            angle=0 * degrees, wavelength_b=(50 * um, 35 * um))

        """

        if isinstance(amplitude, (int, float)):
            amplitude1, amplitude2 = (amplitude, amplitude)
        else:
            amplitude1, amplitude2 = amplitude

        if isinstance(y0, (int, float)):
            y0_1, y0_2 = (y0, -y0)
        else:
            y0_1, y0_2 = y0

        if isinstance(wavelength_b, (int, float)):
            wavelength_b1, wavelength_b2 = (wavelength_b, wavelength_b)
        else:
            wavelength_b1, wavelength_b2 = wavelength_b

        assert amplitude1 > 0 and amplitude2 > 0 and wavelength_b1 > 0 and wavelength_b2 > 0

        # Rotacion del circula/elipse
        Xrot, Yrot = self.__rotate__(angle)

        u = np.zeros_like(self.X)
        Y_sin1 = amplitude1 * np.sin(2 * np.pi * Xrot / wavelength_b1) + y0_1
        Y_sin2 = amplitude2 * np.sin(2 * np.pi * Xrot / wavelength_b2 +
                                     phase) + y0_2
        ipasa_1 = (Yrot < Y_sin1) and (Yrot > Y_sin2)
        u[ipasa_1] = 1
        self.u = u

    def hiper_ellipse(self,
                      r0=(0 * um, 0 * um),
                      radius=(50 * um, 50 * um),
                      angle=0 * degrees,
                      n=(2, 2)):
        """
        This module will create a hiper_ellipse mask

        References:
            https://en.wikipedia.org/wiki/Superellipse

        Args:
            r0 (float, float): center of super_ellipse
            radius (float, float): radius of the super_ellipse
            angle (float): angle of rotation in radians
            n (float, float) =  degrees of freedom of the next equation, n = (n1, n2)
                    |(Xrot - x0) / radiusx|^n1 + |(Yrot - y0) / radiusy|=n2

                    i.e.:
                        n1 = n2 = 1: for a square
                        n1 = n2 = 2: for a circle
                        n1 = n2 = 0.5: for a super-ellipse

        Example:

            hiper_ellipse(r0=(0 * um, 0 * um), radius=(250 * um, 125 * um), angle=0 * degrees)
        """

        # si solamente un numero, posiciones y radius son los mismos para ambos
        if isinstance(r0, (float, int)):
            x0, y0 = (r0, r0)
        else:
            x0, y0 = r0

        if isinstance(n, (int, float)):
            nx, ny = (n, n)
        else:
            nx, ny = n

        assert nx > 0 and ny > 0

        if isinstance(radius, (float, int)):
            radiusx, radiusy = (radius, radius)
        else:
            radiusx, radiusy = radius

        # Rotation of the super-ellipse
        Xrot, Yrot = self.__rotate__(angle)

        # Definition of transmittance
        u = np.zeros_like(self.X)
        ipasa = np.abs((Xrot - x0) / radiusx)**nx + \
            np.abs((Yrot - y0) / radiusy)**ny < 1
        u[ipasa] = 1
        self.u = u

    def crossed_slits(self, r0=(0 * um, 0 * um), slope=1.0, angle=0 * degrees):
        """
        This function will create a crossed slit mask

        Args:
            r0 (float, float): center of the crossed slit
            slope (float, float): slope of the slit
            angle (float): Angle of rotation of the slit

        Example:

            crossed_slits(r0 = (-10 * um, 20 * um), slope = 2.5, angle = 30 * degrees)
        """
        if isinstance(slope, (float, int)):
            slope_x, slope_y = (slope, slope)
        else:
            slope_x, slope_y = slope

        if isinstance(r0, (float, int)):
            x0, y0 = (r0, r0)
        else:
            x0, y0 = r0

        # Rotation of the crossed slits
        Xrot, Yrot = self.__rotate__(angle)

        u = np.zeros_like(self.X)
        Y1 = slope_x * np.abs(Xrot - x0) + y0
        Y2 = slope_y * np.abs(Xrot - x0) + y0

        if (slope_x > 0) and (slope_y < 0):
            ipasa = (Yrot > Y1) | (Yrot < Y2)
        elif (slope_x < 0) and (slope_y > 0):
            ipasa = (Yrot < Y1) | (Yrot > Y2)
        elif (slope_x < 0) and (slope_y < 0):
            Y2 = -Y2 + 2 * y0
            ipasa = (Yrot < Y1) | (Yrot > Y2)
        else:
            Y2 = -Y2 + 2 * y0
            ipasa = (Yrot > Y1) | (Yrot < Y2)

        u[ipasa] = 1
        self.u = u

    def triangle(self, r0=None, slope=2.0, height=50 * um, angle=0 * degrees):
        """
        This module will create a triangle mask. It uses the equation of a straight line:

                                y = -slope * |x - x0| + y0

        Args:

            r0 (float, float): Coordinates of the top corner of the triangle
            slope (float): Slope if the equation above
            height (float): Distance between the top corner of the triangle and
                            the basis of the triangle
            angle (float): Angle of rotation of the triangle
        """
        if isinstance(r0, (float, int)):
            x0, y0 = (r0, r0)
        elif r0 == None:
            x0 = 0 * um
            y0 = height / 2
        else:
            x0, y0 = r0

        # Rotation of the super-ellipse
        Xrot, Yrot = self.__rotate__(angle)

        Y = -slope * np.abs(Xrot - x0) + y0
        u = np.zeros_like(self.X)

        ipasa = (Yrot < Y) & (Yrot > y0 - height)
        u[ipasa] = 1
        u[u > 1] = 1
        self.u = u

    def sinusoidal_slits(self,
                         N=3,
                         amplitude=10 * um,
                         d=10 * um,
                         phase=0 * degrees,
                         angle=0 * degrees,
                         wavelength_b=25 * um):
        """
        This function will create a sinusoidal mask of N wave-like slits

        Args:
            N (int): Number of sinusoidal slits
            amplitude (float): Amplitude of the wave-like slit
            phase (float): Phase (in radians) between the wave-like slit.
            angle (float): Angle to rotate the sinusoidal slit mask
            wavelength_b (float): Wavelength of the wave-like slits

        Example:

            sinusoidal_slits(N = 3, amplitude = 20 * um,
                            phase = 45 * degrees, angle=0 * degrees,
                            wavelength_b=20 * um)

        """

        YY = np.linspace(self.y.min(), self.y.max(), N + 2)

        u = np.zeros_like(self.X)

        for i, centre_y in enumerate(YY[1:-1]):
            self.sinusoidal_slit(
                y0=(centre_y + d, centre_y - d),
                amplitude=amplitude,
                phase=phase,
                angle=angle,
                wavelength_b=wavelength_b)
            u = u + self.u
        u[u > 1] = 1
        self.u = u

    def muchas_formas(self,
                      D,
                      space=50 * um,
                      margin=50 * um,
                      tol=.5,
                      angle=0 * degrees):
        """
        This module will draw a matrix of the specified form

        Args:
            D (Scalar_mask_XY): Mask of the desired figure to be drawn
            space (float, float): spaces between figures. space = (space in x, space in y)
            margin (float, float): extra space outside the mask
            tol (float): difference between the points selected to draw the figures and the points of the meshgrid
            angle (float): Angle to rotate the matrix of circles

        Example:
            A = Scalar_mask_XY(x, y, wavelength)
            A.ring(r0, radius1, radius2, angle)
            muchas_formas2(D = A, space = 50 * um, angle = 0 * degrees)
        """

        if isinstance(space, (int, float)):
            space_x, space_y = (space, space)
        else:
            space_x, space_y = space

        if isinstance(margin, (float, int)):
            margin_x, margin_y = (margin, margin)
        else:
            margin_x, margin_y = margin

        assert space_x > 0 and space_y > 0 and margin_x >= 0 and margin_y >= 0

        # Rotation of the mask
        Xrot, Yrot = self.__rotate__(angle)

        X0 = Xrot.min() - margin_x
        Y0 = Yrot.min() - margin_y

        u = np.zeros_like(self.X)
        u1 = np.zeros_like(self.X)

        for i in range(50):  # The number 50 could be changed for bigger masks
            X = X0 + i * space_x
            j = 0
            Y = Y0
            if (X > self.x.max() + margin_x):
                break
            else:
                while (Y <= self.y.max() + margin_y):
                    ipasa = (np.abs(Xrot - X) < tol) & (np.abs(Yrot - Y) < tol)
                    u1[ipasa] = 1
                    j += 1
                    Y = Y0 + j * space_y
                    u = u + u1

        u = fftconvolve(u, D.u, mode='same')
        u[u > 1] = 1
        self.u = u

    def batman_mask(self, negativo=True):
        imagen1 = mpimg.imread('batman1.png')
        imgshow = plt.imshow(
            imagen1,
            vmin=0,
            vmax=1,
            aspect='auto',
            extent=[self.x.min(),
                    self.x.max(),
                    self.y.min(),
                    self.y.max()])
        imagen1 = mpfig.Figure()
        imgshow.set_cmap('hot')

        T = Scalar_mask_XY(self.x, self.y, self.wavelength)
        T.u = imagen1
        u = np.zeros_like(self.X)
        u = u + imagen1

        self.u = 1 - u
