

% 22/5/08: Carga los datos en la pantalla real o virtual de una lente con
% un n�mero de zonas N.

%Carga los datos en el holoeye
%dentro del bucle for hay que tomar las medidas experimentales.

N=60;      %numero de zonas
f=0.4;   %focal de la lente en m
lambda=.68e-6;  %longitud de onda
nombreArchivo='LenteCuadradaHoloeye.mat';   %nombre de archivo temporal donde se guardan datos
p=50; % Periodo, en p�xeles, de la red Blaze que se superpone a la imagen original.

GenerarLenteCuadrada(f,lambda,N,nombreArchivo); %Genera la lente de Fresnel  


posreal=[1 1 1024 768];             % Posici�n de la pantalla real
posholoeye=[1025 1 1024 768];     % Posici�n de la pantalla virtual 

ID=figure;  %preparardibujo
%axis equal
%axis square
%for i=4:N
i=N;
    mapafase=generarZonasLente(i,nombreArchivo);    % Genera el mapa que se va a cargar en el modulador 
    
    [matriz_mapa_fase,inicio_x,fin_x,inicio_y,fin_y,ID]=CargarLenteCuadradaSLM(mapafase,posreal,ID,p); %posreal
    
    
   % pause

    %aqui hay que grabar lo que tengamos en la c�mara y guardarlo o procesarlo
    %preguntar Fran sobre programa de captura con ueye.

    %preguntar LuisMiguel, cuando se enfoque sobre qu� hay que capturar.
%end

