function ueye=captura(ueyeCtrl,ueye,nombre)
%
%Luis Miguel S�nchez-Brea - 28/05/2006
%
%reliza captura y la almacena en matriz dentro de ueye.imagen.I
%m�todo lento pues con activeX todavia no se como recoger directamente la
%imagen. Aqu� lo hago a trav�s de saveImage


if nargin==2
   % nombre='c:/vidrio_0p081ms.bmp';
     nombre='c:/prueba_lente_fresnel_camara.bmp';
end
    
SaveImage(ueyeCtrl,nombre);
ueye.imagen.I=double(imread(nombre))';



if ueye.Ctrl.actualizarParametrosImagen==0
ueye.imagen.ruido='';
ueye.imagen.SN='';
ueye.imagen.saturados='';
else
    ueye=parametrosimagen(ueye,0);
end

