% Generaci�n de una superficie rugosa plana

function [t, alturas]= rugosidad2D(parametros,def_rugosidad)


if isempty(def_rugosidad)
    sigma=2*um;   % positivo convergente
    T=200*um;
    modo='transmision'; %o reflexi�n
    indiceRefraccion=1.5;                      %para transmisi�n
else
    sigma=def_rugosidad.sigma;
    T=def_rugosidad.T;
    modo=def_rugosidad.modo;
    indiceRefraccion=def_rugosidad.indiceRefraccion;
end

k=2*pi/parametros.lambda;

%[SX,SY]=ndgrid(parametros.sx,parametros.sy);

deltaX=parametros.sx(2)-parametros.sx(1);
deltaY=parametros.sy(2)-parametros.sy(1);
ancho=parametros.sx(end)-parametros.sx(1);
largo=parametros.sy(end)-parametros.sy(1);
% Da la altura h_corr en micas introduciendo la rugosidad s,
% la long. de correlaci�n t y el paso Delta en micrones,
% junto con el ancho y el largo en micras

% Para la generaci�n de la superficie cil�ndrica rugosa
% se sigue el procedimiento del promedio m�vil (Ogilvy p.224)

% PAR�METROS DE LA SUPERFICIE
L_ancho=ancho/(2*deltaX);
L_largo=largo/(2*deltaY);


M=round(4*T/(sqrt(2)*deltaX)); % Fija la cantidad de pesos: (2M+1)x(2M+1)

N_ancho=floor(L_ancho+M); % Fija la cantidad de alturas
% no correlacionadas sobre
% el ancho: 2Nancho+1

N_largo=floor(L_largo+M); % Fija la cantidad de alturas
% no correlacionadas sobre el largo: 2Nlargo+1

% malla_h_no_corr=[2*N_ancho+1,2*N_largo+1]; % Tama�o previo de la malla de puntos o
%                                     % nodos sobre la superficie
%
% malla_pesos=[2*M+1,2*M+1]; % Tama�o de la malla de
%                            % pesos sobre la superficie
%
% malla_h_corr=[2*(N_ancho-M)+1,2*(N_largo-M)+1]; % Tama�o final de la malla de
% nodos sobre la superficie


% GENERACI�N DE LA SUPERFICIE

% Pesos para correlacionar las alturas:
[desp_ancho,desp_largo]=meshgrid(-M:M); % Desplazamientos para correlacionar
% (dominio del kernel de la correlacion)
desp_ancho=desp_ancho*deltaX;
desp_largo=desp_largo*deltaY;

pesos=exp(-2*(desp_ancho.^2+desp_largo.^2)/(T^2)); % Pesos para la correlacion
cte_norm=sum(sum(pesos.^2));
pesos=pesos/sqrt(cte_norm); % p' q' la suma de sus cuadrados de 1

% Alturas no correlacionadas:
h_no_corr=sigma*randn(2*N_ancho+2,2*N_largo+2); % Alturas no correlacionadas
% con distribucion N(0,sigma)
extension.x=length(h_no_corr(1,:));
extension.y=length(h_no_corr(:,1));
[pesosExtendida,posicionMatriz]=extenderMatriz(pesos, extension);

% Alturas correlacionadas en el plano:
% Las alturas salen de correlacionar entre s� las h no corr
% reemplazando una de ellas por el promedio ponderado con sus vecinas
% tic
%h_corr_conv=conv2(h_no_corr,pesos,'valid'); % Como la fcion pesos es par, las
% toc
h_corr_fft=sqrt(2*pi)*fftshift(ifft2(fft2(h_no_corr).*fft2(pesosExtendida)));
L=length(h_no_corr(1,:))-length(pesos(1,:));
PInicial=(length(pesos(1,:))-1)/2;
alturas=imcrop(h_corr_fft,[PInicial,PInicial,L,L]);

if modo(1)=='r'
    t=exp(-2*i*k*alturas);
elseif modo(1)=='t'
    t=exp(i*k*(indiceRefraccion-1)*alturas);
end

