


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FUNCI�N PARA CARGAR CUALQUIER IMAGEN AL MODULADOR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% JMR
% 5/3/08. Probado. Funciona.

% Esta funci�n carga mediante la pantalla virtual del monitor la 
% m�scara que se desee en el modulador

% LA M�SCARA DEBE ESTAR EN FORMATO UINT8.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Variables de entrada: coordenadas_pant_virtual, nombre_mascara, formato_mascara
% Variables de salida: mascara, asa_figura, coordenadas_pant_virtual 


function [mascara,asa_figura,coordenadas_pant_virtual]=Carga_mascara_SR(coordenadas_pant_virtual,mascara)

% La m�scara debe ser de 1024x768 p�xeles. Las unidades de las coordenadas
% deben ir en p�xeles, empezando en 1 y no en cero. (Son las unidades sin defecto )
% coordenadas_pant_virtual: [-1 -1025 1024 768]  
mascara=uint8(mascara);
%1: Leo la m�scara
%mascara=imread(nombre_mascara,formato_mascara);  % Las variables de entrada son cadenas de caracteres.
%2: Creo la figura en la pantalla virtual
asa_figura=figure;
%3: Fijo las coordenadas de la pantalla virtual
set(asa_figura,'Position',coordenadas_pant_virtual);   % [left bottom width height]
%4: Cargo la imagen en el modulador a trav�s de la pantalla virtual
image(mascara);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





