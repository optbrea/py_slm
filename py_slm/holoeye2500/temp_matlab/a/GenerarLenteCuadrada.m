 function GenerarLenteCuadrada(f,lambdad,nfresnel,nombreArchivo)

% calculo de la geometria de la placa zonal cuadrada optimizada. el numero
% de lados del poligono es 4. 
% lo primero es calcular los tama;os de las zonas circulares de Fresnel
% estos tama;os dependen del indice de refraccion en el que se va a
% propagar la onda, de la longitud de onda de dise;o y de la focal del
% sistema.

if isempty(f)
f=0.100; % en metros
end

if isempty(lambdad)
    lambdad=650e-9; % lambda del diodo laser lambdad=633e-9; % lambda del He-Ne
end

if isempty(nfresnel)
nfresnel=100; % suponemos que es mayor que 100
end

if isempty(nombreArchivo)
    nombreArchivo='lentecuadrada.mat';
end


nfresnelcalculo=nfresnel;


nexterno=1; % se propaga en aire
nmedio=1.6;


indicefresnel=linspace(1,nfresnel,nfresnel);
radiofresnel=sqrt(f.*lambdad.*indicefresnel+(lambdad.*indicefresnel./2).^2);
%anchuraultimazona=radiofresnel(nfresnel)-radiofresnel(nfresnel-1);

% conversion a una placa de Fresnel con forma cuadrada
s=4; % porque tiene 4 caras
xm=radiofresnel./sqrt(1+((tan(pi/s)).^2./4));
%anchuraultimocuadrado=xm(nfresnel)-xm(nfresnel-1);

% debido a problemas de memoria vamos a hacer el calculo para las primeras
% 100 zonas
xm=[0 xm];
anchurazona100=xm(nfresnelcalculo+1)-xm(nfresnelcalculo);
deltax=anchurazona100/10;
x=0:deltax:xm(nfresnelcalculo);
[xx,yy]=meshgrid(x,x);
[npx,npy]=size(xx);
aout=ones(npx,npx).*...
            ((0.5.*(1+f./sqrt(f.^2+xx.^2+yy.^2)))./...
            sqrt(f.^2+xx.^2+yy.^2)).*exp(-i.*(2*pi./lambdad).*nexterno.*sqrt(f.^2+xx.^2+yy.^2));


azone=zeros(nfresnelcalculo,1);

for jj=1:nfresnelcalculo;
indicesexterior=find(and(xx<xm(jj+1),yy<xm(jj+1)));
indicesinterior=find(and(xx<xm(jj),yy<xm(jj)));
azone(jj)=sum(aout(indicesexterior))-sum(aout(indicesinterior));
   disp([jj, length(indicesexterior)- length(indicesinterior)]);
end

%intreferencia=(abs(sum(sum(aout))));
%intmatched=(sum(abs(azone))).^2;
%efficiency=intmatched./intreferencia;

%%

nivelfase=zeros(1,5);
errornivelfase=nivelfase;

fases=angle(azone);
for jj=1:length(nivelfase);
    nivelfase(jj)=mean(fases([1+jj:5:nfresnelcalculo]));
    errornivelfase(jj)=std(fases([1+jj:5:nfresnelcalculo]));
end
nivelfase=[fases(1) nivelfase];
errornivelfase=[0 errornivelfase];
  
%espesor=(nivelfase.*lambdad)./(2*pi*(nmedio-nexterno));
%espesor=espesor-min(espesor);
%errorespesor=(errornivelfase.*lambdad)./(2*pi*(nmedio-nexterno));
%errorespesorglobal=mean(errorespesor(2:6));


save(nombreArchivo, 'xm', 'fases', 'nivelfase', 'errornivelfase', 'f', 'lambdad');
