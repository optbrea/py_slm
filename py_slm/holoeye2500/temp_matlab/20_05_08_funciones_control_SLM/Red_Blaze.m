





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ESTA FUNCI�N CALCULA UNA RED BLAZ� CON UN �NGULO DE INCLINACI�N DADO 
% PARA DESVIAR EL HAZ DEL MODULADOR 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [Red_Blaze_Matriz,Theta,X_Modulador,Y_Modulador]=Red_Blaze(p,x_Modulador,y_Modulador)


% 26/5/08. Sin probar. 
% 30/5/08. Probado. Funciona.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% VARIABLES DE ENTRADA

% VECTOR DE TAMA�O X_MODULADOR: x_MODULADOR
% VECTOR DE TAMA�O Y_MODULADOR: y_MODULADOR
% p, per�odo de la red Blaz�. (Creo que debe ir en p�xeles).



% VARIABLES DE SALIDA

% Red_Blaze_Matriz: MATRIZ DE SALIDA CON LOS VALORES DE LA RED EN LA MALLA
% Theta, �ngulo de inclinaci�n.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

Phase_Max=255; % M�ximo nivel de gris para la fase en el modulador.

% Calculo de la malla 

[X_Modulador,Y_Modulador]=meshgrid(x_Modulador,y_Modulador);

% Calculo de la red Blaze. Se usan niveles de gris [0...255] para el
% modulador en la fase discretizada

Red_Blaze_Matriz=(uint8((Phase_Max/(p-1)).*mod(X_Modulador,p)))';  % Nos encontraremos en el punto dado por el resto, ya que la red es peri�dica. (Ojo, es una matriz) 
Theta=(180/pi)*atan(Phase_Max/(p-1));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%











