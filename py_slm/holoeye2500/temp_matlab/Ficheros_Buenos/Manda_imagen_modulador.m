%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FUNCI�N PARA MANDAR IMAGEN AL MODULADOR LA PANTALLA VIRTUAL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 6/9/08: Sin probar
% Funciona
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [matriz_mapa_fase_correcta]=Manda_imagen_modulador(f,lambdad,nfresnel)

% Variables de entrada:

% f, focal de la lente en m
% lambdad, longitud de onda en m
% nfresnel, n�mero de zonas de Fresnel de la lente

% Variables de salida:

% matriz_mapa_fase_correcta, matriz de fase en p�xeles, RGB uint8.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%1: Se genera el mapa de la lente con el fichero de Javier:

mapafase=f_generamapalente2(f,lambdad,nfresnel);

%2: Se rellenan los p�xeles a los que no se les hab�a asignado valor y se pasa a formato uint8 RGB

[matriz_mapa_fase,inicio_x,fin_x,inicio_y,fin_y]=CargarLenteCuadradaSLM(mapafase);
[matriz_mapa_fase_correcta]=Convierte_a_RGB_uint8(matriz_mapa_fase);

%3: Se manda al modulador la matriz

fullscreen(matriz_mapa_fase_correcta,2);  % La pantalla virtual es la "2"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%







