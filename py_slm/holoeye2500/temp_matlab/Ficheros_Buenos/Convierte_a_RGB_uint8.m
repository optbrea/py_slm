%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FUNCI�N PARA CONVERTIR A FORMATO RGB LA MATRIZ COMPLETA QUE SE VA A PASAR
% AL MODULADOR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 6/9/08: Sin probar.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [matriz_mapa_fase_correcta]=Convierte_a_RGB_uint8(matriz_mapa_fase)

% VARIABLES DE ENTRADA:

% matriz_mapa_fase: es una matriz de 1024 x 768 de doubles
 
% VARIABLES DE SALIDA:

% matriz_mapa_fase_correcta: es una matriz de 768 x 1024 p�xeles de formato RGB,uint8.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Se pasa a uint8 y se transpone

M1 = uint8(matriz_mapa_fase'); % Hay que transponer la matriz, sino da error.

% Se crea la matriz RGB concatenando M1 y d�ndole igual peso a las componentes
% RGB, ya que la matriz original ya est� en grises de [0 255].

matriz_mapa_fase_correcta=cat(3,M1,M1,M1); 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




