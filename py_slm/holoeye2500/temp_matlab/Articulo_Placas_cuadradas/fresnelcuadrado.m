function fresnelcuadrado

% calculo de la geometria de la placa zonal cuadrada optimizada. el numero
% de lados del poligono es 4. 

% lo primero es calcular los tama;os de las zonas circulares de Fresnel
% estos tama;os dependen del indice de refraccion en el que se va a
% propagar la onda, de la longitud de onda de dise;o y de la focal del
% sistema.

f=0.100; % en metros
nexterno=1; % se propaga en aire
lambdad=633e-9; % lambda del He-Ne
lambdad=650e-9; % lambda del diodo laser
nfresnel=1000; % suponemos que es mayor que 100
indicefresnel=linspace(1,nfresnel,nfresnel);
radiofresnel=sqrt(f.*lambdad.*indicefresnel+(lambdad.*indicefresnel./2).^2);
anchuraultimazona=radiofresnel(nfresnel)-radiofresnel(nfresnel-1);

% conversion a una placa de Fresnel con forma cuadrada

s=4; % porque tiene 4 caras
xm=radiofresnel./sqrt(1+((tan(pi/s)).^2./4));
anchuraultimocuadrado=xm(nfresnel)-xm(nfresnel-1);

% debido a problemas de memoria vamos a hacer el calculo para las primeras
% 100 zonas

xm=[0 xm];
anchurazona100=xm(101)-xm(100);
deltax=anchurazona100/10;
x=[0:deltax:xm(100)];
[xx,yy]=meshgrid(x,x);
[npx,npy]=size(xx);
aout=ones(npx,npx).*...
            ((0.5.*(1+f./sqrt(f.^2+xx.^2+yy.^2)))./...
            sqrt(f.^2+xx.^2+yy.^2)).*exp(-i.*(2*pi./lambdad).*nexterno.*sqrt(f.^2+xx.^2+yy.^2));

intreferencia=(abs(sum(sum(aout))));

nfresnelcalculo=100;
for jj=1:nfresnelcalculo;
indicesexterior=find(and(xx<xm(jj+1),yy<xm(jj+1)));
indicesinterior=find(and(xx<xm(jj),yy<xm(jj)));
azone(jj)=sum(aout(indicesexterior))-sum(aout(indicesinterior));
   disp([jj, length(indicesexterior)- length(indicesinterior)]);
end
    
intmatched=(sum(abs(azone))).^2;
efficiency=intmatched./intreferencia;

%%
fases=angle(azone);
for jj=1:5;
    nivelfase(jj)=mean(fases([1+jj:5:nfresnelcalculo]));
    errornivelfase(jj)=std(fases([1+jj:5:nfresnelcalculo]));
end

nivelfase=[fases(1) nivelfase];
errornivelfase=[0 errornivelfase];


nmedio=1.6;
    
espesor=(nivelfase.*lambdad)./(2*pi*(nmedio-nexterno));
espesor=espesor-min(espesor);
errorespesor=(errornivelfase.*lambdad)./(2*pi*(nmedio-nexterno));
errorespesorglobal=mean(errorespesor(2:6));

save parametrosfzpcuadrada xm fases f lambdad nivelfase errornivelfase
